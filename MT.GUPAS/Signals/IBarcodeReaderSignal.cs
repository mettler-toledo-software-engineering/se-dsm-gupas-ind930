﻿namespace MT.GUPAS.Signals
{
    interface IBarcodeReaderSignal
    {
        string Value { get; set; }
    }
}
