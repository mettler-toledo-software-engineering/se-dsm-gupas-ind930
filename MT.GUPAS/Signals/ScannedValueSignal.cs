﻿using System;

using System.Collections.Generic;
using System.Text;

namespace MT.GUPAS.Signals
{
    public class ScannedValueSignal : IBarcodeReaderSignal
    {
        public string Value { get; set; }
    }
}
