﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using MT.Singularity.Logging;
using MT.Singularity.Platform;

namespace MT.GUPAS.Update
{
    public static class SystemUpdate
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(SystemUpdate);

        private static string _destinationPath = SingularityEnvironment.ServiceDirectory;
        private static List<string> _availableUpdateFiles = new List<string>();

        public const string IND930USBDrive = "D:\\";
        public static string SystemUpdateSourceDirectory = Path.Combine(IND930USBDrive, "systemupdate");

        public static bool SystemUpdateFileExists()
        {
            if (CheckDirectoriesExists())
            {
                _availableUpdateFiles = Directory.EnumerateFiles(SystemUpdateSourceDirectory).Select(Path.GetFileName).Where(x => x.Contains(SingularityEnvironment.AppPackageExtension)).ToList();
                bool hasFiles = _availableUpdateFiles.Count > 0;
                return hasFiles;
            }

            return false;
        }

        public static bool ExecuteUpdate()
        {
            bool result = false;
            if (SystemUpdateFileExists())
            {
                foreach (string updateFile in _availableUpdateFiles)
                {
                    result = CopyFile(updateFile);
                    if (result == false)
                    {
                        break;
                    }
                }

            }

            return result;
        }

        private static bool CopyFile(string filename)
        {
            string sourcefile = Path.Combine(SystemUpdateSourceDirectory, filename);
            string destinationfile = Path.Combine(_destinationPath, filename);
            bool result = false;
            try
            {
                File.Copy(sourcefile, destinationfile, true);
                result = true;
            }
            catch (IOException ex)
            {
                Logger.ErrorEx($"could not copy File to Destination Directory. FileName: {filename}, Source: {SystemUpdateSourceDirectory}, Destination: {_destinationPath}", SourceClass, ex);
            }

            return result;
        }


        private static bool CheckDirectoriesExists()
        {
            bool result = true;
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            if (Directory.Exists(_destinationPath) == false)
            {
                result = CreateDirectory(_destinationPath);

            }

            foreach (var drive in allDrives)
            {
                if (drive.DriveType == DriveType.Removable)
                {
                    SystemUpdateSourceDirectory = Path.Combine(drive.Name, "systemupdate");
                    result = Directory.Exists(SystemUpdateSourceDirectory);
                    break;
                }
            }
            
            return result;
        }

        private static bool CreateDirectory(string path)
        {
            bool result = false;
            try
            {
                Directory.CreateDirectory(path);
                result = true;
            }
            catch (IOException ex)
            {
                Logger.ErrorEx($"Update Destination Directory could not be created: {path}", SourceClass, ex);
                result = false;
            }

            return result;
        }
    }
}
