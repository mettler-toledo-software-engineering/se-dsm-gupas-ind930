﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using MT.Singularity.Composition;
using MT.GUPAS.Config;

namespace MT.GUPAS.Data
{
    [Export(typeof(AuditTrailService))]
    public class AuditTrailService
    {
        private CompositionContainer _container;
        private Context _context;
        private Configuration _configuration;
        private IDataBaseConnection<AuditTrailModel> _auditConnection;
        private Dictionary<AuditKlasse, string> KlasseZ = new Dictionary<AuditKlasse, string>()
        {
            {AuditKlasse.Auftrag,"Auftrag"},
            {AuditKlasse.Protokoll,"Protokoll"},
            {AuditKlasse.Tabelle,"Tabelle"},
            {AuditKlasse.Applikation,"Tabelle"},
            {AuditKlasse.Weighing,"Wiegung"}
        };

        private Dictionary<AuditAction, string> ActionZ = new Dictionary<AuditAction, string>()
        {
            {AuditAction.Erstellen,"Erstellen"},
            {AuditAction.Loeschen,"Loeschen" },
            {AuditAction.Beenden,"Beenden" },
            {AuditAction.Ausdrucken,"Ausdrucken" },
            {AuditAction.Starten,"Starten" },
            {AuditAction.Aendern,"Aendern" },
            {AuditAction.Abbrechen,"Abbrechen" }

        };
        public AuditTrailService(CompositionContainer container)
        {
            _container = container;
            Initialize();
        }

        private async void Initialize()
        {
            _auditConnection = _container.Resolve<SqlGenericDataBaseAccess<AuditTrailModel>>();
            var customerComponent = _container.Resolve<IComponents>();
            _configuration = await customerComponent.GetConfigurationAsync();
            _context = _container.Resolve<Context>();
        }

        public void WriteAuditTrial(AuditKlasse a, AuditAction b, string Para1, string Para2)
        {
            string ersteller = $"{_context._selectedUser.Name.Trim()} / Term: {_configuration.TerminalId}";
            AuditTrailModel at = new AuditTrailModel()
            {
                Ersteller = ersteller.Substring(Math.Min(19, ersteller.Length)),
                Zeit = DateTime.Now,
                Klasse = KlasseZ[a],
                Aktion = ActionZ[b],
                Para = Para1.Trim().Substring(0, Math.Min(24, Para1.Trim().Length)),
                Para2 = Para2.Trim().Substring(0, Math.Min(49, Para2.Trim().Length))
            };

            _auditConnection.SaveData(at);
        }
    }
    public enum AuditKlasse
    {
        Auftrag,
        Protokoll,
        Tabelle,
        Applikation,
        Weighing
    }
    public enum AuditAction
    {
        Erstellen,
        Loeschen,
        Beenden,
        Ausdrucken,
        Starten,
        Aendern,
        Abbrechen
    }
}
