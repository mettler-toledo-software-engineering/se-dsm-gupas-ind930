﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Data
{
    public interface IDataBaseConnection<T>
    {
        Task<int> SaveDataAsync(T dataEntry);
        long SaveData(T dataEntry);
        Task<bool> UpdateDataAsnyc(T dataEntry);
        bool UpdateData(T dataEntry);
        Task<bool> DeleteDataAsync(T dataEntry);
        bool DeleteData(T dataEntry);
        bool DeleteDataById(int Id);
        Task<List<T>> GetAllDataEntriesAsync();
        List<T> GetAllDataEntries();
        Task<T> GetEntryByIdAsync(int id);
        T GetEntryById(int id);
        Task<List<T>> GetEntriesByParameterAsync(string parameterName, object parameterValue);
        List<T> GetEntriesByParameter(string parameterName, object parameterValue);
        int GetTableCount();
       
    }
}
