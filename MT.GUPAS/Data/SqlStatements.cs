﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.GUPAS.Config;

namespace MT.GUPAS.Data
{
    public static class SqlStatements
    {
        public static string GetSqlStatement<T>(string parameterName, SqlStatementTypes statementType)
        {
            string sql = "";
            string tablename = $"{typeof(T).Name.ToUpper().Replace("MODEL", "")}";

            //wenn es ein interface ist schneiden wir das I ab
            if (tablename.StartsWith("I") && char.IsUpper(tablename, 1))
            {
                tablename = tablename.Substring(1);
            }

            switch (statementType)
            {
                case SqlStatementTypes.SelectFromWhere:
                    sql = $"select * from [{tablename}] where {parameterName} = @{parameterName} ";
                    break;
                case SqlStatementTypes.DeleteFromWhere:
                    sql = $"delete from [{tablename}] where {parameterName} = @{parameterName} ";
                    break;
                case SqlStatementTypes.CountEntries:
                    sql = $"select count(*) from [{tablename}]";
                    break;
                case SqlStatementTypes.DateTime:
                    sql = $"select concat (convert(varchar, getdate(), 104), ' ', 	convert(varchar, getdate(), 8));"; 
                    break;

            }
            return sql;
        }

        public static string GetSqlStatement<T>(string parameterName1, string parameterName2, SqlStatementTypes statementType)
        {
            string sql = "";
            string tablename = $"{typeof(T).Name}s";

            //wenn es ein interface ist schneiden wir das I ab
            if (tablename.StartsWith("I") && char.IsUpper(tablename, 1))
            {
                tablename = tablename.Substring(1);
            }

            switch (statementType)
            {

                case SqlStatementTypes.SelectBetween:
                    sql = $"select * from [{tablename}] where Zeitstempel BETWEEN @{parameterName1} AND @{parameterName2}";
                    break;
            }
            return sql;
        }
    }
    public enum SqlStatementTypes
    {
        SelectFromWhere,
        SelectBetween,
        DeleteFromWhere,
        CountEntries,
        DateTime
    }

}
