﻿using System;

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using log4net;
using log4net.Util;
using MT.GUPAS.Data;
using MT.GUPAS.Logic;
using MT.Singularity.Composition;
using MT.Singularity.Logging;

namespace MT.GUPAS.Data
{
    [Export]
    public class SqlGenericDataBaseAccess<T> : IDataBaseConnection<T> where T : class
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(SqlGenericDataBaseAccess<T>);

        public Task<bool> DeleteDataAsync(T dataEntry)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                    {
                        var result = await cnn.DeleteAsync(dataEntry);
                        //   Logger.InfoEx($"deleted {dataEntry} of type {typeof(T).Name}", SourceClass);

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorEx($"delete {dataEntry} of type {typeof(T).Name} failed", SourceClass, ex);
                    return false;
                }

            });
            return task;
        }
        public bool DeleteData(T dataEntry)
        {

            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var result = cnn.Delete(dataEntry);
                    // Logger.InfoEx($"deleted {dataEntry} of type {typeof(T).Name}", SourceClass);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"delete {dataEntry} of type {typeof(T).Name} failed", SourceClass, ex);
                return false;
            }
        }

        public bool DeleteDataById(int Id)
        {
            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var p = new DynamicParameters();

                    string parameterName = "RecipeHeaderId";
                    p.Add($"@{parameterName}", Id);

                    string sql = SqlStatements.GetSqlStatement<T>(parameterName, SqlStatementTypes.DeleteFromWhere);
                    cnn.Query<T>(sql, p);
                    Logger.InfoEx($"executed query{sql}");
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"delete of type {typeof(T).Name} failed", SourceClass, ex);
                return false;
            }

        }

      public int GetTableCount()
        {
            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {

                    string sql = SqlStatements.GetSqlStatement<T>(null, SqlStatementTypes.CountEntries);
                    int count = Convert.ToInt32(cnn.ExecuteScalar(sql));
                    return count;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"executed count on {typeof(T).Name} failed", SourceClass, ex);
                return 0;
            }
        }

      public string GetDateTime()
      {
          try
          {
              using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
              {

                  string sql = SqlStatements.GetSqlStatement<T>(null, SqlStatementTypes.DateTime);
                  string result = Convert.ToString(cnn.ExecuteScalar(sql));
                  return result;
              }
          }
          catch (Exception ex)
          {
              Logger.ErrorEx($"executed get datetime on {typeof(T).Name} failed", SourceClass, ex);
              return "";
          }
      }
        public Task<List<T>> GetAllDataEntriesAsync()
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                    {
                        var result = await cnn.GetAllAsync<T>();

                        //      Logger.InfoEx($"got all data entries of type { typeof(T).Name}", SourceClass);

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorEx($"getting all entries of type {typeof(T).Name} from database failed", SourceClass, ex);
                    return null;
                }

            });
            return task;
        }

        public List<T> GetAllDataEntries()
        {

            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var result = cnn.GetAll<T>();

                    //    Logger.InfoEx($"got all data entries of type { typeof(T).Name}", SourceClass);

                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"getting all entries of type {typeof(T).Name} from database failed", SourceClass, ex);
                return null;
            }
        }

        public Task<T> GetEntryByIdAsync(int id)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                    {
                        var result = await cnn.GetAsync<T>(id);

                        //       Logger.InfoEx($"got data with with {id} result : {result}", SourceClass);

                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorEx($"getting {typeof(T).Name} with id {id} from database failed", SourceClass, ex);
                    return null;
                }

            });
            return task;
        }

        public T GetEntryById(int id)
        {
            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var result = cnn.Get<T>(id);

                    //     Logger.InfoEx($"got data with with {id} result : {result}", SourceClass);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"getting {typeof(T).Name} with id {id} from database failed", SourceClass, ex);
                return null;
            }
        }

        public Task<int> SaveDataAsync(T dataEntry)
        {
            var task = Task.Run(async () =>
          {
              try
              {
                  using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                  {
                      var result = await cnn.InsertAsync(dataEntry);

                      //    Logger.InfoEx($"{dataEntry} saved to database", SourceClass);

                      return result;
                  }
              }
              catch (Exception ex)
              {
                  Logger.ErrorEx($"saving {dataEntry} to database failed", SourceClass, ex);
                  return 0;
              }

          });
            return task;
        }

        public long SaveData(T dataEntry)
        {
            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var result = cnn.Insert(dataEntry);

                    // Logger.InfoEx($"{dataEntry} saved to database", SourceClass);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"saving {dataEntry} to database failed", SourceClass, ex);
                return 0;
            }

        }

        public Task<List<int>> SaveAllDataAsync(List<T> dataEntries)
        {
            var task = Task.Run(async () =>
            {
                List<int> resultList = new List<int>();
                try
                {
                    using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                    {
                        foreach (T dataEntry in dataEntries)
                        {
                            var result = await cnn.InsertAsync(dataEntry);
                            resultList.Add(result);

                            //           Logger.InfoEx($"{dataEntry} saved to database", SourceClass);
                        }
                        return resultList;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorEx($"saving multiple {typeof(T).Name} to database failed", SourceClass, ex);
                    return resultList;
                }

            });
            return task;
        }

        public Task<bool> UpdateDataAsnyc(T dataEntry)
        {
            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                    {
                        var result = await cnn.UpdateAsync(dataEntry);
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorEx($"update {dataEntry} to database failed", SourceClass, ex);
                    return false;
                }

            });
            return task;
        }
        public bool UpdateData(T dataEntry)
        {

            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var result = cnn.Update(dataEntry);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"update {dataEntry} to database failed", SourceClass, ex);
                return false;
            }

        }

        public Task<List<T>> GetEntriesByParameterAsync(string parameterName, object parameterValue)
        {


            var task = Task.Run(async () =>
            {
                try
                {
                    using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                    {
                        var p = new DynamicParameters();

                        if (parameterValue is int)
                        {
                            int param = (int)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else if (parameterValue is string)
                        {
                            string param = (string)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else if (parameterValue is double)
                        {
                            double param = (double)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else if (parameterValue is DateTime)
                        {
                            DateTime param = (DateTime)parameterValue;
                            p.Add($"@{parameterName}", param);
                        }
                        else
                        {
                            Logger.ErrorEx($"unknwon paramter type used {(parameterValue)} with paramter {parameterName}", SourceClass);
                        }

                        string sql = SqlStatements.GetSqlStatement<T>(parameterName, SqlStatementTypes.SelectFromWhere);

                        var result = await cnn.QueryAsync<T>(sql, p);

                        //         Logger.InfoEx($"got data with with {parameterName} and value {parameterValue} result : {result}", SourceClass);

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorEx($"getting {typeof(T).Name} with paramter {parameterName} and value {parameterValue} from database failed", SourceClass, ex);
                    return null;
                }

            });
            return task;
        }

        public List<T> GetEntriesByParameter(string parameterName, object parameterValue)
        {
            
            try
            {
                using (IDbConnection cnn = new SqlConnection(Globals.CnnString()))
                {
                    var p = new DynamicParameters();

                    if (parameterValue is int)
                    {
                        int param = (int)parameterValue;
                        p.Add($"@{parameterName}", param);
                    }
                    else if (parameterValue is string)
                    {
                        string param = (string)parameterValue;
                        p.Add($"@{parameterName}", param);
                    }
                    else if (parameterValue is double)
                    {
                        double param = (double)parameterValue;
                        p.Add($"@{parameterName}", param);
                    }
                    else if (parameterValue is DateTime)
                    {
                        DateTime param = (DateTime)parameterValue;
                        p.Add($"@{parameterName}", param);
                    }
                    else
                    {
                        Logger.ErrorEx($"unknwon paramter type used {(parameterValue)} with paramter {parameterName}", SourceClass);
                    }

                    string sql = SqlStatements.GetSqlStatement<T>(parameterName, SqlStatementTypes.SelectFromWhere);

                    var result = cnn.Query<T>(sql, p);

                    //       Logger.InfoEx($"got data with with {parameterName} and value {parameterValue} result : {result}", SourceClass);

                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"getting {typeof(T).Name} with paramter {parameterName} and value {parameterValue} from database failed", SourceClass, ex);
                return null;
            }


        }




    }
}
