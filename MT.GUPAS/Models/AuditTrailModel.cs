﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("AuditTrail")]
    public class AuditTrailModel
    {
        [Key]
        public int Nummer { get; set; }
        public string Klasse { get; set; }
        public string Aktion { get; set; }
        public DateTime Zeit { get; set; }
        public string Ersteller { get; set; }
        public string Para { get; set; }
        public string Para2 { get; set; }


        public AuditTrailModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer}";
        }
    }
}
