﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("ADetail")]
    public class ADetailModel 
    {
        [Key]
        public int Nummer { get; set; }
        public int Lot { get; set; }
        public double Soll { get; set; }
        public double SollP { get; set; }
        public double SollM { get; set; }
        public string ZielLot { get; set; }
        public int Verwieger { get; set; }
        public double Netto { get; set; }
        public double Brutto { get; set; }
        public double Tara { get; set; }
        public int Hand { get; set; }
        public DateTime Zeit { get; set; }
        public int Status { get; set; }
        public int Kabine { get; set; }
        public int Waage { get; set; }
        public int VonEti { get; set; }
        public int BisEti { get; set; }
        public double Tara2 { get; set; }

        public ADetailModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer}";
        }
    }
}
