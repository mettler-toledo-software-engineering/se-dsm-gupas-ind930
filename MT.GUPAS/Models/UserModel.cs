﻿using System;
using System.Data;

namespace MT.GUPAS.Models
{
    
    public class UserModel 
    {
        
        public int Nummer { get; set; }
        public int Account { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public string PW { get; set; }
        public int Level { get; set; }
        
        public UserModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Name}  / {Tag}, {Nummer}";
        }
    }
}
