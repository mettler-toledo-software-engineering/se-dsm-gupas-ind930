﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("Etikette")]
    public class EtiketteModel 
    {
        [Key]
        public int Nummer { get; set; }
        public int Detail { get; set; }
        public int EtiNr { get; set; }
        
        
        public EtiketteModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer} / {EtiNr}";
        }
    }
}
