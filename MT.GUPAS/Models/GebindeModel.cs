﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("Gebinde")]
    public class GebindeModel 
    {
        [Key]
        public int Nummer { get; set; }
        public int Detail { get; set; }
        public string Gebinde { get; set; }
        public double Menge { get; set; }
        public int Status { get; set; }


        public GebindeModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer} / {Gebinde}";
        }
    }
}
