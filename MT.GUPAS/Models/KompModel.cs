﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("Komp")]
    public class KompModel 
    {
        [Key]
        public int Nummer { get; set; }
        public string Name { get; set; }
        public string ArtNr { get; set; }
        public string GenesysNr { get; set; }
        public int Active { get; set; }


        public KompModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer} / {Name}";
        }
    }
}
