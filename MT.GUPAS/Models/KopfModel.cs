﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("AKopf")]
    public class KopfModel 
    {
        [Key]
        public int Nummer { get; set; }
        public string ANr { get; set; }
        public int MG { get; set; }
        public string BArt { get; set; }
        public string Besteller { get; set; }
        public int Ersteller { get; set; }
        public int AutoEti { get; set; }
        public DateTime ZeitNew { get; set; }
        public double Total { get; set; }
        public int Kabine { get; set; }
        public int Komplett { get; set; }
        public int AnzZeilen { get; set; }
        public int AnzZeilenTotal { get; set; }
        public int EtiketteNr { get; set; }
        public int AnzPrint { get; set; }
        
        public KopfModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer}";
        }
    }
}
