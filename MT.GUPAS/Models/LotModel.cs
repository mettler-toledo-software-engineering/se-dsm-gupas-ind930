﻿using System;
using System.Data;
using Dapper.Contrib.Extensions;

namespace MT.GUPAS.Models
{
    [Table("ALot")]
    public class LotModel 
    {
        [Key]
        public int Nummer { get; set; }
        public int Auftrag { get; set; }
        public string Lot { get; set; }
        
        
        public LotModel()
        {
            Nummer = 0;
        }

       public override string ToString()
        {
            return $"{Nummer} / {Lot}";
        }
    }
}
