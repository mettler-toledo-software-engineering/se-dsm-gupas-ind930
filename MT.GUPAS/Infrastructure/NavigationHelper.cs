﻿using MT.GUPAS.View;
using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.GUPAS.Infrastructure
{
    [Export]
    public class NavigationHelper
    {
        private ISignalManager _signalManager;
        private readonly CompositionContainer _container;
        private ISecurityService _securityService;

        public NavigationHelper(ISignalManager signalManager, ISecurityService securityService, AnimatedNavigationFrame targetFrame, CompositionContainer container)
        {
            _signalManager = signalManager;
            _securityService = securityService;
            _container = container;
            _signalManager.RegisterCallback<CommonUxSignal>(Callback);

            TargetFrame = targetFrame;
        }

        private void Callback(object sender, CommonUxSignal signal)
        {
            Navigate(signal);
        }

        private AnimatedNavigationFrame _targetFrame;

        public AnimatedNavigationFrame TargetFrame
        {
            get { return _targetFrame; }
            set { _targetFrame = value; }
        }

        private AnimatedNavigationFrame _targetAnimationFrame;

        public AnimatedNavigationFrame TargetAnimationFrame
        {
            get { return _targetAnimationFrame; }
            set { _targetAnimationFrame = value; }
        }


        private void Navigate(CommonUxSignal signal)
        {
            switch (signal.MessageType)
            {
                case CommonUxSignalType.NavigateBack:
                    TargetFrame.Back();
                    break;

                case CommonUxSignalType.NavigateToPage:
                    switch (signal.Tag.ToString())
                    {
                        //case "BlankPage":
                        //    TargetFrame.NavigateTo(new BlankPage());
                        //    break;
                        //case "SettingsPage":
                        //    TargetFrame.NavigateTo(new SettingsPage(_signalManager));
                        //    break;
                        //case "UserManagementPage":
                        //    TargetFrame.NavigateTo(_container.Resolve<UserMaintenancePage>());
                        //    break;
                        //case "ArticleManagementPage":
                        //    TargetFrame.NavigateTo(new ArticleMaintenancePage(_signalManager, _securityService));
                        //    break;
                        //case "Weighing":
                        //    TargetFrame.NavigateTo(_container.Resolve<DosingMix>());
                        //    break;
                        //case "ChooseArticle":
                        //    TargetFrame.NavigateTo(new ChooseArticlePage(_signalManager, _securityService));
                        //    break;
                      
                    }
                    break;
            }
        }
    }
}
