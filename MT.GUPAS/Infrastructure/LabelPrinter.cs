﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Serialization;
using MT.GUPAS.Config;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using MT.Singularity.Collections;

namespace MT.GUPAS.Infrastructure
{
    public class LabelPrinter
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(LabelPrinter);
        private static StringSerializer stringSerializer;
        private static DelimiterSerializer delimiterSerializer;

        private List<String> printData = new List<string>();
        private ISignalManager _signalManager;
        private Components _customerComponents;

        public LabelPrinter(ISignalManager signalManager, Components customerComponents)
        {
            _customerComponents = customerComponents;
            _signalManager = signalManager;
        }

        public static async Task<LabelPrinter> CreateAsync(ISignalManager signalManager, Components customerComponents)
        {
            var newInstance = new LabelPrinter(signalManager, customerComponents);
            await newInstance.InitializeAsync();

            return newInstance;
        }

        public async Task InitializeAsync()
        {
            var compositionContainer = ApplicationBootstrapperBase.CompositionContainer;
            var platformEngine = compositionContainer.Resolve<IPlatformEngine>();
            var interfaceService = await platformEngine.GetInterfaceServiceAsync();

            try
            {
                var allInterfaces = await interfaceService.GetAllInterfacesAsync();

                ISerialInterface serialInterface = allInterfaces.OfType<ISerialInterface>()
                    .FirstOrDefault(item => item.LogicalPort == 5);
                if (serialInterface == null)
                {
                    Logger.Error("Error: serial port 5 not installed");
                }
                else
                {
                    IConnectionChannel<DataSegment> actualConnectionChannel =
                        await serialInterface.CreateConnectionChannelAsync();
                    if (actualConnectionChannel != null)
                    {
                        delimiterSerializer =
                            new DelimiterSerializer(actualConnectionChannel, CommonDataSegments.Newline);
                        stringSerializer = new StringSerializer(delimiterSerializer);
                        if (stringSerializer != null)
                        {
                            stringSerializer.CodePage = CodePage.CP850;
                            try
                            {
                                await stringSerializer.OpenAsync();
                                Logger.ErrorEx("Test port 5", SourceClass);
                            }
                            catch (Exception ex)
                            {
                                Logger.ErrorEx("Error opening serial port 5", SourceClass, ex);
                                await stringSerializer.CloseAsync();
                                throw;
                            }

                            await stringSerializer.CloseAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx("Error CreateConnection RS232", SourceClass, ex);
            }
        }

        private async Task Print(List<string> lines)
        {
            foreach (String s in lines)
            {
                if (!String.IsNullOrEmpty(s))
                {
                    try
                    {
                        await stringSerializer.WriteAsync(s);
                        Thread.Sleep(50);
                    }
                    catch (Exception ex)
                    {
                        Logger.ErrorEx("Error printing line", SourceClass, ex);
                    }
                }
            }
        }


        public async Task<bool> PrintLabel(ADetailModel selADetail, KopfModel selKopf, KompModel selKomp, Context context)
        {
            string fileName = Path.Combine(Globals.GetDataDirectory(), "GUPAS.txt");
            string templateData = "";

            try
            {
                if (File.Exists(fileName))
                {
                    templateData = File.ReadAllText(fileName, Encoding.GetEncoding(1252));
                }
                else
                {
                    Logger.ErrorEx($"No template at {fileName}", SourceClass);
                    return false;
                }

                if ((selKomp.GenesysNr != null) && (selKomp.GenesysNr.Trim().Length > 0))
                {
                    templateData = templateData.Replace("{KGNR}", selKomp.GenesysNr.Trim());
                    templateData = templateData.Replace("{KGNRB}", selKomp.GenesysNr.Trim());
                }
                else
                {
                    templateData = templateData.Replace("{KGNR}", "N/A");
                    templateData = templateData.Replace("{KGNRB}", "{KILL}");
                }
                if ((selKomp.ArtNr != null) && (selKomp.ArtNr.Trim().Length > 0))
                {
                    templateData = templateData.Replace("{KNR}", selKomp.ArtNr.Trim());
                    templateData = templateData.Replace("{KNRB}", selKomp.ArtNr.Trim());
                }
                else
                {
                    templateData = templateData.Replace("{KNR}", "N/A");
                    templateData = templateData.Replace("{KNRB}", "{KILL}");
                }
                
                templateData = templateData.Replace("{ALOT}", selADetail.ZielLot.Trim());
                templateData = templateData.Replace("{KNAME}", selKomp.Name.Trim());
                templateData = templateData.Replace("{ANR}", selKopf.ANr.Trim());
                templateData = templateData.Replace("{ABEM}", selKopf.BArt.Trim());
                templateData = templateData.Replace("{BTAG}", context._selectedUser.Name.Trim());
                templateData = templateData.Replace("{ENR}", selADetail.BisEti.ToString());
                templateData = templateData.Replace("{ABEST}", selKopf.Besteller.Trim());
                templateData = templateData.Replace("{DATUM}", selADetail.Zeit.ToString("dd.MM.yy HH:mm"));
                templateData = templateData.Replace("{GROSS}", string.Format("{0,10:0.000}", selADetail.Netto + selADetail.Tara));
                templateData = templateData.Replace("{NET}", string.Format("{0,10:0.000}", selADetail.Netto));
                templateData = templateData.Replace("{TARA}", string.Format("{0,10:0.000}", selADetail.Tara));

                printData.Clear();
                printData.AddRange(templateData.Split('\n'));
                int i = printData.FindIndex(x => x.Contains("{KILL}"));
                if (i > -1)
                    printData.RemoveAt(i);

                Logger.Info("Print Label");
                await stringSerializer.OpenAsync();
                await Print(printData);
                await stringSerializer.CloseAsync();
            }
            catch (Exception ex)
            {
                Logger.ErrorEx(
                    $"Print of label {selKopf.EtiketteNr} weighing {selADetail.Nummer} failed !",
                    SourceClass, ex);
            }

            return true;

        }
    }
}


