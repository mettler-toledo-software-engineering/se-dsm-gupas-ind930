﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using MT.GUPAS.Barcode;
using MT.GUPAS.Config;
using MT.GUPAS.Data;
using MT.GUPAS.Logic;
// ReSharper disable once RedundantUsingDirective
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.UserInteraction;
using MT.Singularity.Presentation.Utils;
using MT.Singularity.Presentation.Model;

namespace MT.GUPAS.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }
        /// <summary>
        /// Initializes the application in this method. This method is called after all
        /// services have been started and before the <see cref="T:MT.Singularity.Platform.Infrastructure.IShell" /> is shown.
        /// </summary>
        protected override async void InitializeApplication()
        {
            base.InitializeApplication();
            await InitializeCustomerService();
        }

        /// <summary>
        /// Initializes the customer service.
        /// Put all customer services or components initialization code in this method.
        /// </summary>
        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var signalManager = CompositionContainer.Resolve<ISignalManager>();
                var scaleService = CompositionContainer.Resolve<IScaleService>();

                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<IComponents>(customerComponent);
                CompositionContainer.AddInstance(await LabelPrinter.CreateAsync(signalManager, customerComponent));
#if !DEBUG
                CompositionContainer.AddInstance(await BarcodeReader.CreateAsync(signalManager, customerComponent));
#else
                CompositionContainer.AddInstance(await ZebraBarcodeReader.CreateAsync(signalManager, customerComponent));
#endif

            }

            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);
                //throw;
            }

        }
    }
}
