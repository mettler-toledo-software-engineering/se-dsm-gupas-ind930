﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MT.GUPAS.Barcode;
using MT.GUPAS.Config;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Configuration.Scale;
using MT.Singularity.Platform.Devices.Scale;

namespace MT.GUPAS.Infrastructure
{
    
    public class ScaleConfigProvider
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(ScaleConfigProvider);

    
        private readonly ISignalManager _signalManager;
        private readonly IScaleService _scaleService;
        private ScaleConfiguration[] _scaleConfiguration;
        private List<int> q = new List<int>();
        private int x;
        public ScaleConfigProvider(IScaleService scaleService, ISignalManager signalManager)
        {
            _scaleService = scaleService;
            _signalManager = signalManager;
        }
        public static async Task<ScaleConfigProvider> CreateAsync(IScaleService scaleService,ISignalManager signalManager)
        {
            var newInstance = new ScaleConfigProvider(scaleService ,signalManager);
            await newInstance.InitializeAsync();
            return newInstance;
        }

        private async Task InitializeAsync()
        {
            var _myScales = await _scaleService.GetAllScalesAsync();
            foreach (var item in _myScales)
            {
                Logger.InfoEx(item.IsInitialized ? "IJa" : "INein", SourceClass);
                Logger.InfoEx(item.IsPairingSuccessful ? "PJa" :"PNein", SourceClass);
                Logger.InfoEx(item.ScaleError.ToString() , SourceClass);
            }

           

            Logger.InfoEx("Hugo", SourceClass);

        }
     
    }
}
