﻿using System;
using System.Runtime.InteropServices;


namespace MT.GUPAS.Infrastructure
{
    public static class MyTimeDate
    {
        //[DllImport("coredll.dll")]
        //private static extern uint SetSystemTime(ref SYSTEMTIME lpSystemTime);

        [DllImport("kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true)]
        public extern static bool SetSystemTime(ref SystemDate sysDate);
        

        public struct SystemDate
        {
            public ushort Year;
            public ushort Month;
            public ushort DayOfWeek;
            public ushort Day;
            public ushort Hour;
            public ushort Minute;
            public ushort Second;
            public ushort Millisecond;
        };

        public static bool SetTime(string dateTime)
        {
            SystemDate systime = new SystemDate();
            string myDate = dateTime.Split(' ')[0];
            string myTime = dateTime.Split(' ')[1];
            systime.Year = Convert.ToUInt16(myDate.Split('.')[2]);
            systime.Month = Convert.ToUInt16(myDate.Split('.')[1]);
            systime.Day = Convert.ToUInt16(myDate.Split('.')[0]);
            systime.Hour = Convert.ToUInt16(myTime.Split(':')[0]);
            systime.Minute = Convert.ToUInt16(myTime.Split(':')[1]);
            systime.Second = Convert.ToUInt16(myTime.Split(':')[2]);
            systime.Millisecond = 0;

            return SetSystemTime(ref systime);

        }


    }
}
