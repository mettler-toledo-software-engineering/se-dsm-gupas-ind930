﻿using System;
using MT.GUPAS.Data;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

using MT.GUPAS.Logic;


namespace MT.GUPAS.View
{
    /// <summary>
    /// View model for <see cref="StartPage"/>.
    /// </summary>
    public class StartPageViewModel : ObservableObject
    {
        private Visual _parent;
        private CompositionContainer _container;
        private Context _context;
        private AuditTrailService _auditTrailService;
        private INavigationService _navigationService;
        public string ProgVersion => Globals.ProjectNumber + "\n" + Globals.ProgVersionStr(false);


        /// <summary>
        /// Initializes a new instance of the <see cref="StartPageViewModel"/> class.
        /// In order to use the parameters stored in the configuration database in your home screen, 
        /// you need to pass the SignalManager to this class. Because the component who manages the 
        /// configuration is created after the home screen creation, so you can use the SignalManager
        /// to receive the LoadCompleted message, then you can get the component and it's configuration.
        /// </summary>
        /// <param name="parent">The Visual Parent of this Page.</param>
        //   public StartPageViewModel(Visual parent, IScaleService scaleService, ISignalManager signalManager, ISecurityService securityService)
        public StartPageViewModel(Visual parent, CompositionContainer container, INavigationService navigationService)
        {
            _navigationService = navigationService;
            _container = container;
            _auditTrailService = _container.Resolve<AuditTrailService>();
            _context = _container.Resolve<Context>();
            _parent = parent;
        }

        #region ---- GoContinue

        public ICommand GoContinue
        {
            get { return new DelegateCommand(DoGoContinue); }
        }

        public async void DoGoContinue()
        {
            if (Globals.DBServer() == false)
            {
                await InfoPopup.ShowAsync(_parent, "Fehler", "Keine Verbindung zum DB-Server", MessageBoxIcon.Error, MessageBoxButtons.OK);
                return;
            }

            var loginSettings = new Login(_parent, _container);
            loginSettings.Closed += loginSettings_Closed;
            loginSettings.Show(_parent);
        }

        private void loginSettings_Closed(object sender, EventArgs e)
        {
            if (((Login)sender).DialogResult == DialogResult.OK)
            {
                _auditTrailService.WriteAuditTrial(AuditKlasse.Applikation,AuditAction.Starten,_context._selectedUser.Name,"");
                _navigationService.NavigateTo(_container.Resolve<Dosing>());
            }
        }
        #endregion

        #region ---- GoContinue
        public ICommand GoSettings
        {
            get { return new DelegateCommand(DoGoSettings); }
        }

        private void DoGoSettings()
        {
            //   _navigationService.NavigateTo(_container.Resolve<Settings>());   

        }
        #endregion

    }
}


