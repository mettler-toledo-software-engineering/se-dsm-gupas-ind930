﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using MT.GUPAS.Config;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using MT.GUPAS.View;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using MT.GUPAS.Data;
using MT.Singularity.Logging;
using log4net;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Signals;
using MT.Singularity;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using System.Timers;
using MT.Singularity.Reflection.Cecil;

namespace MT.GUPAS.ViewModel
{
    [SuppressMessage("ReSharper", "ArrangeAccessorOwnerBody")]
    public class DosingViewModel : PropertyChangedBase
    {
        #region private variables

        private readonly Visual _parent;

        private StateEngine _state;
        private StateEngine _lastState;
        private static readonly string SourceClass = nameof(DosingViewModel);
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private readonly Timer _logOutTimer = new Timer(1000);
        private WeightInformation[] _weightInfo = new WeightInformation[5];
        private IScale[] _myScales;
        private bool _inTolerance = false;
        private int _lastActiveScale;
        private string _lastMessage;
        private int[] _dispResolution = new int[5];
        private int _terminalId;
        private int _timeLeftConfigured;
        private bool _inprocess = false;
        private double _acceptanceMin;
        private double _acceptanceMax;
        private double _alreadyDosed;
        private double _tara2;
        private string _scannedNr = "";
        private string _requestPreTare = "";
        private string _requestPreTare2 = "";

        private Configuration _configuration;
        private CompositionContainer _compositionContainer;
        private Context _context;
        private INavigationService _navigationService;
        private ISignalManager _signalManager;
        private IScaleService _scaleService;
        private AuditTrailService _auditTrailService;
        private LabelPrinter _printerService;


        private IDataBaseConnection<ADetailModel> _detailConnection;
        private IDataBaseConnection<LotModel> _lotConnection;
        private IDataBaseConnection<KopfModel> _kopfConnection;
        private IDataBaseConnection<KompModel> _kompConnection;
        private IDataBaseConnection<GebindeModel> _gebindeConnection;
        private IDataBaseConnection<EtiketteModel> _etiketteConnection;

        private ADetailModel _selectedADetail;
        private LotModel _selectedLot;
        private KopfModel _selectedKopf;
        private KompModel _selectedKomp;
        private GebindeModel _selectedGebinde;
        //private List<GebindeModel> _selectedGebindes;

        private StateDosage _stateDosage;
        #endregion

        public DosingViewModel(Visual parent, CompositionContainer container, INavigationService navigationService)
        {
            _parent = parent;
            _navigationService = navigationService;
            _compositionContainer = container;
            Initialize();
        }

        #region Initialize

        private async void Initialize()
        {
            _state = StateEngine.Init;
            var customerComponent = _compositionContainer.Resolve<IComponents>();
            _configuration = await customerComponent.GetConfigurationAsync();
            _context = _compositionContainer.Resolve<Context>();
            _scaleService = _compositionContainer.Resolve<IScaleService>();
            _auditTrailService = _compositionContainer.Resolve<AuditTrailService>();
            _signalManager = _compositionContainer.Resolve<ISignalManager>();
            _printerService = _compositionContainer.Resolve<LabelPrinter>();
            _detailConnection = _compositionContainer.Resolve<SqlGenericDataBaseAccess<ADetailModel>>();
            _lotConnection = _compositionContainer.Resolve<SqlGenericDataBaseAccess<LotModel>>();
            _kopfConnection = _compositionContainer.Resolve<SqlGenericDataBaseAccess<KopfModel>>();
            _kompConnection = _compositionContainer.Resolve<SqlGenericDataBaseAccess<KompModel>>();
            _gebindeConnection = _compositionContainer.Resolve<SqlGenericDataBaseAccess<GebindeModel>>();
            _etiketteConnection = _compositionContainer.Resolve<SqlGenericDataBaseAccess<EtiketteModel>>();

            _stateDosage = StateDosage.Init;

            _myScales = await _scaleService.GetAllScalesAsync();
            if ((_myScales.Length > 0) && (_myScales[0] != null))
            {
                _myScales[0].NewWeightInHostUnit += DosingViewModel_NewWeightInHostUnit;
                _weightInfo[1] = await _myScales[0].GetWeightAsync(UnitType.Host);
            }

            if ((_myScales.Length > 1) && (_myScales[1] != null))
            {
                _myScales[1].NewChangedWeightInHostUnit += DosingViewModel_NewChangedWeightInHostUnit;
                _weightInfo[2] = await _myScales[1].GetWeightAsync(UnitType.Host);
            }

            if ((_myScales.Length > 2) && (_myScales[2] != null))
            {
                _myScales[2].NewChangedWeightInHostUnit += DosingViewModel_NewChangedWeightInHostUnit2;
                _weightInfo[3] = await _myScales[2].GetWeightAsync(UnitType.Host);
            }

            if ((_myScales.Length > 3) && (_myScales[3] != null))
            {
                _myScales[3].NewChangedWeightInHostUnit += DosingViewModel_NewChangedWeightInHostUnit3;
                _weightInfo[4] = await _myScales[3].GetWeightAsync(UnitType.Host);
            }

            _signalManager.RegisterCallback<IBarcodeReaderSignal>(OnCustomSignal);
            _dispResolution[1] = 3;
            _dispResolution[2] = 4;
            _dispResolution[3] = 7;
            _dispResolution[4] = 3;


            ActiveScale = _scaleService.SelectedScale.ScaleNumber;
            _terminalId = _configuration.TerminalId;
            _timeLeftConfigured = _configuration.AutoLogoffTime; // in sec

            RenewTimeLeft();
            _logOutTimer.AutoReset = true;
            if (_timeLeftConfigured > 0)
                _logOutTimer.Start();
            _logOutTimer.Elapsed += _logOutTimer_Elapsed;


            ClearTare();
        }

        private void _logOutTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimeLeft--;
            if (TimeLeft < 0)
            {
                ReleaseAllEvents();
                _navigationService.Back();
            }
        }

        public void RenewTimeLeft()
        {
            TimeLeft = _timeLeftConfigured;
        }

        private void OnCustomSignal(object sender, IBarcodeReaderSignal signal)
        {
            if (signal.GetType() == typeof(ScannedValueSignal))
            {
                _scannedNr = signal.Value;
            }
        }

        private void DosingViewModel_NewChangedWeightInHostUnit(WeightInformation weight)
        {
            if (weight.State != WeightState.Busy)
            {
                _weightInfo[2] = weight;
            }
        }

        private void DosingViewModel_NewChangedWeightInHostUnit2(WeightInformation weight)
        {
            if (weight.State != WeightState.Busy)
            {
                _weightInfo[3] = weight;
            }
        }
        private void DosingViewModel_NewChangedWeightInHostUnit3(WeightInformation weight)
        {
            if (weight.State != WeightState.Busy)
            {
                _weightInfo[4] = weight;
            }
        }
        #endregion


        #region * * * STATE ENGINE * * *

        async void DosingViewModel_NewWeightInHostUnit(WeightInformation weight)
        {
            int _intResult = 0;

            if (weight.State != WeightState.Busy)
                _weightInfo[1] = weight;

            ActiveScale = _scaleService.SelectedScale.ScaleNumber;

            ActiveScale = _scaleService.SelectedScale.ScaleNumber;
            if ((_lastActiveScale != ActiveScale) || (_requestPreTare.Length > 0))
            {
                _requestPreTare2 = _requestPreTare;
                _requestPreTare = "";
                _lastState = _state;
                _lastActiveScale = ActiveScale;
                _lastMessage = Message;
                _state = StateEngine.PreInit;
            }



            if (_myScales.Length > 0 && _weightInfo[1] == null && _myScales[0] != null)
                return;
            if (_myScales.Length > 1 && _weightInfo[2] == null && _myScales[1] != null)
                return;
            if (_myScales.Length > 2 && _weightInfo[3] == null && _myScales[2] != null)
                return;
            if (_myScales.Length > 3 && _weightInfo[4] == null && _myScales[3] != null)
                return;

            double _tempWeight = (_weightInfo[ActiveScale].TareWeight > 0)
                ? _weightInfo[ActiveScale].NetWeight
                : _weightInfo[ActiveScale].GrossWeight;

            DisplayedWeight = _tempWeight;


            switch (_state)
            {
                case StateEngine.PreInit:
                    Message = "Bitte warten...";
                    _state = StateEngine.PreInit2;
                    break;
                case StateEngine.PreInit2:
                    if (_weightInfo[ActiveScale].IsValid)
                    {
                        Logger.ErrorEx($"Aktive Scale {ActiveScale}, Error {_myScales[ActiveScale - 1].ScaleError}",
                            SourceClass);
                        SetInfos();
                        if (_requestPreTare2.Length > 0)
                        {
                            await _scaleService.SelectedScale.PresetTareAsync(_requestPreTare2, _weightInfo[ActiveScale].Unit);
                            _selectedADetail.Hand = 1;
                            _requestPreTare2 = "";
                        }
                        Message = _lastMessage;
                        _state = _lastState;
                    }
                    break;
                case StateEngine.Init:
                    _scannedNr = "";
                    _tara2 = 0;
                    Charge = Container = Localization.Get(Localization.Key.NA);
                    SetTolerance(false);
                    DeltaTracVisibility = Visibility.Hidden;
                    Message = Localization.Get(Localization.Key.PosLong);
                    EnterValueText = Localization.Get(Localization.Key.EnterPosition);
                    EnterValueVisibility = Visibility.Visible;
                    EnterValueLayout = InputDialog.Layout.Numeric;
                    EnterValueValue = "";
                    _state = StateEngine.WaitOnPosNr;
                    break;
                case StateEngine.WaitOnPosNr:
                    if (_scannedNr.Length > 0)
                    {
                        if (Int32.TryParse(_scannedNr, out _intResult))
                        {
                            _selectedADetail =
                                _detailConnection.GetEntryById(
                                    Int32.Parse(_scannedNr.Substring(0, Math.Min(_scannedNr.Length, 8))));
                            if ((_selectedADetail != null) && (_selectedADetail.Status == (int)StateDosage.Init))
                            {
                                _scannedNr = "";
                                _selectedLot = _lotConnection.GetEntryById(_selectedADetail.Lot);
                                _selectedKopf = _kopfConnection.GetEntryById(_selectedLot.Auftrag);
                                _selectedKomp = _kompConnection.GetEntryById(_selectedKopf.MG);
                                _alreadyDosed = 0;
                                _stateDosage = StateDosage.Init;
                                ClearTare();
                                // get all infos
                                if ((_selectedKopf.Kabine & _configuration.TerminalId) == 0)
                                    await InfoPopup.ShowAsync(_parent, "Fehler", "Auftrag für Kabine gesperrt!",
                                        MessageBoxIcon.Warning, MessageBoxButtons.OK);
                                else
                                {
                                    EnterValueText = Localization.Get(Localization.Key.EnterMaterial);
                                    EnterValueLayout = InputDialog.Layout.Numeric;
                                    EnterValueValue = "";
                                    Message = Localization.Get(Localization.Key.MaterialLong);
                                    _state = StateEngine.WaitOnMaterialNr;
                                }
                            }
                            else
                            {
                                _scannedNr = "";
                                await InfoPopup.ShowAsync(_parent, "Fehler", "Wiegeauftrag nicht gefunden!",
                                    MessageBoxIcon.Warning, MessageBoxButtons.OK);

                                // _navigationService.Back();
                            }
                        }
                        else
                        {
                            _scannedNr = "";
                            await InfoPopup.ShowAsync(_parent, "Fehler", "Wiegeauftrag muss numerisch sein!",
                                MessageBoxIcon.Warning, MessageBoxButtons.OK);
                        }
                    }
                    break;
                case StateEngine.WaitOnMaterialNr:
                    if (_scannedNr.Length > 0)
                    {

                        if ((_selectedKomp.ArtNr != null && _scannedNr.Trim() == _selectedKomp.ArtNr.Trim()) ||
                            (_selectedKomp.GenesysNr != null && (_scannedNr.Trim() == _selectedKomp.GenesysNr.Trim())))
                        {
                            _scannedNr = "";
                            EnterValueText = Localization.Get(Localization.Key.EnterLot);
                            EnterValueLayout = InputDialog.Layout.AlphaNumeric;
                            EnterValueValue = "";
                            Message = Localization.Get(Localization.Key.LosLong);
                            _state = StateEngine.WaitOnChargeNr;
                        }
                        else
                        {
                            _scannedNr = "";
                            await InfoPopup.ShowAsync(_parent, "Fehler", "Falsche Artikel/Genesys-Nr!", MessageBoxIcon.Warning,
                                MessageBoxButtons.OK);
                        }
                    }
                    break;
                case StateEngine.WaitOnChargeNr:
                    if (_scannedNr.Length > 0)
                    {
                        if (_scannedNr.Trim() == _selectedLot.Lot.Trim())
                        {
                            _scannedNr = "";
                            Message = Localization.Get(Localization.Key.ContainerLong);
                            EnterValueText = Localization.Get(Localization.Key.EnterContainer);
                            EnterValueLayout = InputDialog.Layout.AlphaNumeric;
                            EnterValueValue = "";
                            _state = StateEngine.WaitOnContainerNr;
                        }
                        else
                        {
                            _scannedNr = "";
                            await InfoPopup.ShowAsync(_parent, "Fehler", "Falsche Lot-Nr!", MessageBoxIcon.Warning, MessageBoxButtons.OK);
                        }
                    }
                    break;
                case StateEngine.WaitOnContainerNr:
                    if ((_scannedNr.Length > 0) && _weightInfo[ActiveScale].IsValid)
                    {
                        _selectedGebinde = _gebindeConnection.GetEntriesByParameter("Detail", _selectedADetail.Nummer)
                            .FirstOrDefault(d => d.Gebinde.Trim() == _scannedNr.Trim());


                        if (_selectedGebinde != null) // Gebinde gefunden
                        {
                            _scannedNr = "";
                            EnterValueText = Localization.Get(Localization.Key.EnterPreTare);
                            EnterValueLayout = InputDialog.Layout.Numeric;
                            EnterValueValue = "";
                            if (_stateDosage == StateDosage.Init)
                            {
                                await _scaleService.SelectedScale.ClearTareAsync();
                                await _scaleService.SelectedScale.ZeroAsync();
                                Message = Localization.Get(Localization.Key.ContAndTare);
                                _state = StateEngine.WaitOnLoadScale;
                            }
                            else
                            {
                                Message = Localization.Get(Localization.Key.WeighIn);
                                DeltaTracVisibility = Visibility.Visible;
                                _state = StateEngine.PrepareDeltaTrac;
                            }
                        }
                        else
                        {
                            if (_stateDosage == StateDosage.Mangel)
                            {
                                var _gebindeNr = _scannedNr;  // muss wegen await zwischengespeichert werden
                                _scannedNr = "";
                                var result = await InfoPopup.ShowAsync(_parent, "Gebinde", "Neues Gebinde ?",
                                    MessageBoxIcon.Question,
                                    MessageBoxButtons.YesNo);
                                if (result == DialogResult.Yes) // No  wiederhohlt implizit
                                {
                                    var _newGebinde = new GebindeModel() { Detail = _selectedADetail.Nummer, Gebinde = _gebindeNr, Status = 1, Menge = 0 };
                                    _gebindeConnection.SaveData(_newGebinde);
                                    EnterValueText = Localization.Get(Localization.Key.EnterMaterial);
                                    Message = Localization.Get(Localization.Key.MaterialLong);
                                    _state = StateEngine.WaitOnMaterialNr;
                                }
                                _scannedNr = "";
                            }
                            else
                            {
                                _scannedNr = "";  // Implizite neue Eingabe
                                await InfoPopup.ShowAsync(_parent, "Fehler", "Falsche Gebinde-Nr!", MessageBoxIcon.Warning, MessageBoxButtons.OK);
                            }
                        }
                    }
                    break;


                case StateEngine.WaitOnLoadScale:
                    if (_weightInfo[ActiveScale].IsValid)
                    {
                        if (_weightInfo[ActiveScale].GetTareWeightIn(WellknownWeightUnit.Gram) > 1)
                        {
#if DEBUG
                            if (ActiveScale == 1)
#else
                            if (ActiveScale == 4)
#endif
                            {
                                _tara2 = -_weightInfo[ActiveScale].GetTareWeightIn(WeightUnits.Kilogram.Unit);
                                Message = Localization.Get(Localization.Key.SecondTareLong);
                                _state = StateEngine.WaitSecondTare;
                            }
                            else
                            {
                                DeltaTracVisibility = Visibility.Visible;
                                _stateDosage = StateDosage.Normal;
                                _state = StateEngine.PrepareDeltaTrac;
                            }
                        }
                    }
                    break;
                case StateEngine.WaitSecondTare:
                    if (_weightInfo[ActiveScale].GetTareWeightIn(WellknownWeightUnit.Kilogram) > Math.Abs(_tara2))
                    {
                        _tara2 += _weightInfo[ActiveScale].GetTareWeightIn(WeightUnits.Kilogram.Unit);
                        DeltaTracVisibility = Visibility.Visible;
                        _stateDosage = StateDosage.Normal;
                        _state = StateEngine.PrepareDeltaTrac;
                    }
                    break;
                case StateEngine.PrepareDeltaTrac:
                    if (_weightInfo[ActiveScale].IsValid)
                    {
                        SetInfos();
                        Message = Localization.Get(Localization.Key.WeighIn);
                        _state = StateEngine.ToleranceCheck;
                    }
                    break;
                case StateEngine.ToleranceCheck:
                    SetTolerance(IsInTolerance());
                    break;
            }
        }

        #endregion

        #region Scale

        private async void ClearTare()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
        }

        #endregion

        #region Helper Methods


        #region SetInfos

        [SuppressMessage("ReSharper", "SpecifyACultureInStringConversionExplicitly")]
        private void SetInfos()
        {
            double _target = 0;
            if (_weightInfo[ActiveScale] == null || _selectedADetail == null)
                return;

            int _resolution = _dispResolution[ActiveScale];
            string _formatString = String.Concat("{0:F", _resolution, "}");


            _target = WeightUnits.Convert(_selectedADetail.Soll,
                WeightUnits.GetWeightUnit(Globals.UOM),
                WeightUnits.GetWellknownWeightUnit(_weightInfo[ActiveScale].Unit));


            Target = string.Format(_formatString, _target);

            var _neg = WeightUnits.Convert(_selectedADetail.SollM - _selectedADetail.Soll,
                WeightUnits.GetWellknownWeightUnit(WellknownWeightUnit.Kilogram),
                WeightUnits.GetWellknownWeightUnit(_weightInfo[ActiveScale].Unit));
            var _pos = WeightUnits.Convert(_selectedADetail.SollP - _selectedADetail.Soll,
                WeightUnits.GetWellknownWeightUnit(WellknownWeightUnit.Kilogram),
                WeightUnits.GetWellknownWeightUnit(_weightInfo[ActiveScale].Unit));

            UpperTolerance = string.Format(_formatString, Math.Round(_pos, _resolution));
            LowerTolerance = string.Format(_formatString, Math.Round(_neg, _resolution));
            _acceptanceMax = _target + _pos;
            _acceptanceMin = _target + _neg;

            int i = _detailConnection.GetEntriesByParameter("Lot", _selectedLot.Nummer)
                .Count(d => d.Status == (int)StateDosage.Init);

            Appearance = DeltaTracAppearance.BarGraph;

            RecipeNumber = $"{(_selectedKomp.ArtNr != null ? _selectedKomp.ArtNr.Trim() : _selectedKomp.GenesysNr.Trim())} / {_selectedKomp.Name}";
            RecipeDescription = $"{_selectedKopf.ANr.Trim()} / {_selectedKopf.BArt.Trim()} / {_selectedADetail.Nummer}";
            RecipeSize = String.Concat(Target, " ", _weightInfo[ActiveScale].Unit.ToAbbreviation());
            DeltaTracTitle = String.Concat("Zielmenge: ", RecipeSize);
            ChargeNumber = $"{_selectedADetail.Nummer} / { _selectedLot.Lot}";
        }

        #endregion

        #region SetTolerance

        private void SetTolerance(bool flag)
        {
            _inTolerance = flag;
            _goDoGoDoneCommand?.NotifyCanExecuteChanged();
        }

        #endregion

        #region IsInTolerance

        private bool IsInTolerance()
        {

            //return (DisplayedWeight >= _acceptanceMin) && (DisplayedWeight <= _acceptanceMax);
            return (DisplayedWeight <= _acceptanceMax);
        }

        #endregion

        #region EnterValue

        /// <summary>
        /// GoBack
        /// </summary>
        public ICommand GoEnterValue
        {
            get { return new DelegateCommand(DoGoEnterValue); }
        }

        public async void DoGoEnterValue()
        {
            InputDialog nd;

            if (EnterValueText == Localization.Get(Localization.Key.EnterPreTare))
                nd = new InputDialog(EnterValueLayout, EnterValueText + $" [{_weightInfo[ActiveScale].Unit.ToAbbreviation()}]", EnterValueValue);
            else
                nd = new InputDialog(EnterValueLayout, EnterValueText, EnterValueValue);

            nd.Width = 1100;

            var dialogResult = await nd.ShowAsync(_parent);
            if (dialogResult == DialogResult.OK)
            {
                if (EnterValueText == Localization.Get(Localization.Key.EnterPreTare))
                {
                    _requestPreTare = nd.Value.Trim();
                }
                else
                    _scannedNr = nd.Value;
            }
        }

        #endregion

        #region EnterValueLayout

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public InputDialog.Layout EnterValueLayout
        {
            get { return _enterValueLayout; }
            set
            {
                if (_enterValueLayout != value)
                {
                    _enterValueLayout = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private InputDialog.Layout _enterValueLayout = InputDialog.Layout.Numeric;

        #endregion

        #region EnterValueVisibility

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility EnterValueVisibility
        {
            get { return _enterValueVisibility; }
            set
            {
                if (_enterValueVisibility != value)
                {
                    _enterValueVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _enterValueVisibility = Visibility.Hidden;

        #endregion

        #region EnterValueText

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string EnterValueText
        {
            get { return _enterValueText; }
            set
            {
                if (_enterValueText != value)
                {
                    _enterValueText = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _enterValueText = Localization.Get(Localization.Key.NA);

        #endregion

        #region EnterValueValue

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string EnterValueValue
        {
            get { return _enterValueValue; }
            set
            {
                if (_enterValueValue != value)
                {
                    _enterValueValue = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _enterValueValue = Localization.Get(Localization.Key.NA);

        #endregion

        #region Charge

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string Charge
        {
            get { return _charge2; }
            set
            {
                if (_charge2 != value)
                {
                    _charge2 = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _charge2 = Localization.Get(Localization.Key.NA);

        #endregion

        #region Container

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string Container
        {
            get { return _container2; }
            set
            {
                if (_container2 != value)
                {
                    _container2 = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _container2 = Localization.Get(Localization.Key.NA);

        #endregion

        #region Back

        /// <summary>
        /// GoBack
        /// </summary>
        public ICommand GoBack
        {
            get { return new DelegateCommand(DoGoBack); }
        }

        private async void DoGoBack()
        {
            DialogResult result;
            double _netto = 0;
            double _brutto = 0;
            double _tara = 0;
            if (_weightInfo[ActiveScale].IsValid)
            {
                _netto = _weightInfo[ActiveScale].GetNetWeightIn(WeightUnits.Kilogram.Unit); // PopUp in Eichmode!
                _brutto = _weightInfo[ActiveScale].GetGrossWeightIn(WeightUnits.Kilogram.Unit); // PopUp in Eichmode!
                _tara = _weightInfo[ActiveScale].GetTareWeightIn(WeightUnits.Kilogram.Unit); // PopUp in Eichmode!

                result = await InfoPopup.ShowAsync(_parent, "Beenden", "Wollen Sie die Abfüllung unterbrechen?",
                    MessageBoxIcon.Question, MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    result = await InfoPopup.ShowAsync(_parent, "Beenden",
                        "Wollen Sie die Abfüllung abbrechen? (Wiegeposition wird blockiert!)",
                        MessageBoxIcon.Question, MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        _selectedGebinde.Menge += (_netto - _alreadyDosed);
                        _selectedGebinde.Status = 3;
                        _gebindeConnection.UpdateData(_selectedGebinde);
                        _stateDosage = StateDosage.Abort;
                        await SaveInformation(_stateDosage, _brutto, _netto, _tara);
                        _auditTrailService.WriteAuditTrial(AuditKlasse.Weighing, AuditAction.Abbrechen,
                            _selectedADetail.Nummer.ToString(), $"Auftrag: {_selectedKopf.ANr}");
                        Logger.InfoEx($"Container {_selectedGebinde.Nummer} of Order {_selectedKopf.ANr.Trim()} Amount {_selectedGebinde.Menge} Abort stored",
                            SourceClass);

                    }
                    else
                        return;
                }

                ReleaseAllEvents();
                _navigationService.Back();
            }
        }

        private void ReleaseAllEvents()
        {
            if (_myScales.Length > 0)
                _myScales[0].NewWeightInHostUnit -= DosingViewModel_NewWeightInHostUnit;
            if (_myScales.Length > 1)
                _myScales[1].NewChangedWeightInHostUnit -= DosingViewModel_NewChangedWeightInHostUnit;
            if (_myScales.Length > 2)
                _myScales[2].NewChangedWeightInHostUnit -= DosingViewModel_NewChangedWeightInHostUnit2;
            _signalManager.UnregisterCallback<IBarcodeReaderSignal>(OnCustomSignal);
            _logOutTimer.Elapsed -= _logOutTimer_Elapsed;
        }

        #endregion

        #region Done

        private bool CanExecuteGoDoneCommand()
        {
            return _inTolerance && !_inprocess;
        }

        private DelegateCommand _goDoGoDoneCommand;
        /// <summary>
        /// Done
        /// </summary>
        public ICommand GoDone => _goDoGoDoneCommand = new DelegateCommand(DoGoDone, CanExecuteGoDoneCommand);


        private async void DoGoDone()
        {
            _inprocess = true;
            DialogResult result;
            double _amountDosed = 0;

            if (_weightInfo[ActiveScale].IsValid)
            {
                // Erfassen bevor Dialog, sonst NetWeight = 0 wegen Eichung !
                _amountDosed = _weightInfo[ActiveScale].GetNetWeightIn(WeightUnits.Kilogram.Unit);
                if (Math.Round(DisplayedWeight, _dispResolution[ActiveScale]) <
                    Math.Round(_acceptanceMin, _dispResolution[ActiveScale]))

                {

                    result = await InfoPopup.ShowAsync(_parent,
                        Localization.Get(Localization.Key.MatMangel_TitleQuestion),
                        Localization.Get(Localization.Key.MatMangel_ContentQuestion),
                        MessageBoxIcon.Question, MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        _selectedGebinde.Menge += (_amountDosed - _alreadyDosed);
                        _alreadyDosed = _amountDosed;
                        _selectedGebinde.Status = 3;
                        _gebindeConnection.UpdateData(_selectedGebinde);
                        Logger.InfoEx($"Container {_selectedGebinde.Nummer} of Order {_selectedKopf.ANr.Trim()} Amount {_selectedGebinde.Menge} LoM stored",
                            SourceClass);

                        Message = Localization.Get(Localization.Key.ContainerLong);
                        EnterValueText = Localization.Get(Localization.Key.EnterContainer);
                        EnterValueValue = "";
                        EnterValueLayout = InputDialog.Layout.Numeric;
                        DeltaTracVisibility = Visibility.Hidden;
                        EnterValueVisibility = Visibility.Visible;
                        _stateDosage = StateDosage.Mangel;
                        _state = StateEngine.WaitOnContainerNr;
                    }
                }
                else
                {
                    _selectedGebinde.Menge += (_amountDosed - _alreadyDosed);
                    _selectedGebinde.Status = 3;
                    _gebindeConnection.UpdateData(_selectedGebinde);
                    Logger.InfoEx($"Container {_selectedGebinde.Nummer} of Order {_selectedKopf.ANr.Trim()} Amount {_selectedGebinde.Menge} stored",
                        SourceClass);

                    _stateDosage = StateDosage.Normal;
                    await SaveInformation(_stateDosage);
                    _auditTrailService.WriteAuditTrial(AuditKlasse.Weighing, AuditAction.Beenden,
                        _selectedADetail.Nummer.ToString(), $"Auftrag: {_selectedKopf.ANr}");
                    ClearTare();
                    if (_selectedKopf.AnzZeilen > 0)
                        _state = StateEngine.Init;
                    else
                    {
                        ReleaseAllEvents();
                        _navigationService.Back();
                    }

                }

                _inprocess = false;
            }
        }

        private async Task<bool> SaveInformation(StateDosage stateDosage, double brutto = 0, double netto = 0, double tara = 0)
        {
            DialogResult result;

            netto = (netto == 0) ? _weightInfo[ActiveScale].GetNetWeightIn(WeightUnits.Kilogram.Unit) : netto;
            brutto = (brutto == 0) ? _weightInfo[ActiveScale].GetGrossWeightIn(WeightUnits.Kilogram.Unit) : brutto;
            tara = (tara == 0) ? _weightInfo[ActiveScale].GetTareWeightIn(WeightUnits.Kilogram.Unit) : tara;

            if (_selectedADetail.ZielLot.Trim() == "*") _selectedADetail.ZielLot = _selectedLot.Lot.Trim();
            _selectedADetail.Verwieger = _context._selectedUser.Nummer;
            _selectedADetail.Kabine = _terminalId;
            _selectedADetail.Waage = ActiveScale;
            _selectedADetail.Zeit = DateTime.Now;
            _selectedADetail.Netto += netto;
            _selectedADetail.Brutto = brutto;
            _selectedADetail.Tara2 = _tara2;
            _selectedADetail.Tara = tara - _tara2;

            _selectedADetail.Status = (int)_stateDosage;
            _selectedADetail.VonEti = 0;
            _selectedADetail.BisEti = 0;
            _detailConnection.UpdateData(_selectedADetail);

            _selectedKopf.AnzZeilen--;
            _selectedKopf.Komplett = (_selectedKopf.AnzZeilen == 0) ? 1 : 0;
            _kopfConnection.UpdateData(_selectedKopf);

            //if (stateDosage == StateDosage.Abort) // bei Abbruch nur ADetail und AKopf updaten
            //{
            //    Logger.InfoEx($"Detail {_selectedADetail.Nummer} of Order {_selectedKopf.ANr.Trim()} cancel", SourceClass);
            //    return true;
            //}

            _selectedADetail.VonEti = _selectedKopf.EtiketteNr;
            _selectedADetail.BisEti = _selectedADetail.VonEti;

            var autoEtikette = _selectedKopf.AutoEti > 0;
            Message = "druckt...";
            do
            {
                if (autoEtikette)
                {
                    result = DialogResult.Yes;
                    autoEtikette = false;
                }
                else
                    result = await InfoPopup.ShowAsync(_parent, Localization.Get(Localization.Key.Print_TitleQuestion),
                            String.Format(Localization.Get(Localization.Key.Print_ContentQuestion),
                                SelectedRecipeItemName),
                            MessageBoxIcon.Question, MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {

                    await _printerService.PrintLabel(_selectedADetail, _selectedKopf, _selectedKomp, _context);
                    EtiketteModel eti = new EtiketteModel()
                    { Detail = _selectedADetail.Nummer, EtiNr = _selectedADetail.BisEti };
                    _etiketteConnection.SaveData(eti);
                    _selectedADetail.BisEti++;

                }
            } while (result == DialogResult.Yes);

            if (_selectedKopf.EtiketteNr != _selectedADetail.BisEti)
            {
                _selectedKopf.EtiketteNr = _selectedADetail.BisEti;
                _selectedADetail.BisEti--;
                _detailConnection.UpdateData(_selectedADetail);
                _kopfConnection.UpdateData(_selectedKopf);
            }

            Logger.InfoEx($"Detail {_selectedADetail.Nummer} of Order {_selectedKopf.ANr.Trim()} stored", SourceClass);

            return true;
        }
        #endregion

        #region Target

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public string Target
        {
            get { return _target; }
            set
            {
                if (_target != value)
                {
                    _target = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _target = "3";

        #endregion

        #region LowerTolerance

        /// <summary>
        /// Gets or sets the lower tolerance.
        /// </summary>
        /// <value>
        /// The lower tolerance.
        /// </value>
        public string LowerTolerance
        {
            get { return _lowerTolerance; }
            set
            {
                // ReSharper disable once RedundantCheckBeforeAssignment
                if (_lowerTolerance != value)
                {
                    _lowerTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _lowerTolerance = "-1";

        #endregion

        #region UpperTolerance

        /// <summary>
        /// Gets or sets the upper tolerance.
        /// </summary>
        /// <value>
        /// The upper tolerance.
        /// </value>
        public string UpperTolerance
        {
            get { return _upperTolerance; }
            set
            {
                if (_upperTolerance != value)
                {
                    _upperTolerance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _upperTolerance = "2";

        #endregion

        #region Appereance

        /// <summary>
        /// Gets or sets the appearance.
        /// </summary>
        /// <value>
        /// The appearance.
        /// </value>
        public DeltaTracAppearance Appearance
        {
            get { return _appearance; }
            set
            {
                if (_appearance != value)
                {
                    _appearance = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DeltaTracAppearance _appearance = DeltaTracAppearance.BarGraph;

        #endregion DeltaTrac

        #region DeltaTracTitle
        public string DeltaTracTitle
        {
            get { return _deltaTracName; }
            set
            {
                if (value != _deltaTracName)
                {
                    _deltaTracName = value;
                    NotifyPropertyChanged();
                }
            }

        }
        private string _deltaTracName;

        #endregion

        #region SelectedRecipeItemName
        public string SelectedRecipeItemName
        {
            get { return _selectedRecipeItemName; }
            set
            {
                if (value != _selectedRecipeItemName)
                {
                    _selectedRecipeItemName = value;
                    NotifyPropertyChanged();
                }
            }

        }
        private string _selectedRecipeItemName;

        #endregion

        #region ActiveScale

        private int _activeScale;

        public int ActiveScale
        {
            get { return _activeScale; }
            set
            {
                if (value != _activeScale)
                {
                    _activeScale = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region TimeLeft

        private int _timeLeft;

        public int TimeLeft
        {
            get { return _timeLeft; }
            set
            {
                if (value != _timeLeft)
                {
                    _timeLeft = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Message

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string Message
        {
            get { return _message; }
            set
            {
                if (_message != value)
                {
                    _message = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _message = Localization.Get(Localization.Key.BitteWarten);

        #endregion

        #region RecipeNumber

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string RecipeNumber
        {
            get { return _recipeNumber; }
            set
            {
                if (_recipeNumber != value)
                {
                    _recipeNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _recipeNumber = "";

        #endregion

        #region RecipeDescription

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string RecipeDescription
        {
            get { return _recipeDescription; }
            set
            {
                if (_recipeDescription != value)
                {
                    _recipeDescription = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _recipeDescription = "";

        #endregion

        #region RecipeSize

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string RecipeSize
        {
            get { return _recipeSize; }
            set
            {
                if (_recipeSize != value)
                {
                    _recipeSize = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _recipeSize = "";

        #endregion

        #region DisplayedWeight

        /// <summary>
        /// Show Text in YML
        /// </summary>
        private double _displayedWeight = 0;

        public double DisplayedWeight
        {
            get { return _displayedWeight; }
            set
            {
                if (_displayedWeight != value)
                {
                    _displayedWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region ChargeNumber

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public string ChargeNumber
        {
            get { return _chargeNumber; }
            set
            {
                if (_chargeNumber != value)
                {
                    _chargeNumber = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _chargeNumber = "";

        #endregion

        #region DeltaTracVisibility

        /// <summary>
        /// Show Text in YML
        /// </summary>
        public Visibility DeltaTracVisibility
        {
            get { return _deltaTracVisibility; }
            set
            {
                if (_deltaTracVisibility != value)
                {
                    _deltaTracVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _deltaTracVisibility = Visibility.Hidden;

        #endregion

        #endregion
    }
}

