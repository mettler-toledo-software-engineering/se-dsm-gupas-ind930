﻿using System.Diagnostics;
using MT.GUPAS.Logic;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Configuration.Scale;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace MT.GUPAS.ViewModel
{
    public class ScalePanelViewModel : PropertyChangedBase
    {
        private readonly IScaleService _scaleService;
        
        public ScalePanelViewModel(Visual parent, ISignalManager signalManager, IScaleService scaleService)
        {
            _scaleService = scaleService;

            
            SelectedScaleVisibility = _scaleService.NumberOfScales > 1 ? Visibility.Visible : Visibility.Hidden ;
            ZeroVisibility = Visibility.Collapsed;

            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewWeightInDisplayUnit;
            _scaleService.NumberOfScalesChanged += ScaleServiceOnNumberOfScalesChanged;
        }

        private void ScaleServiceOnNumberOfScalesChanged()
        {
            SelectedScaleVisibility = _scaleService.NumberOfScales > 1 ? Visibility.Visible : Visibility.Hidden;
        }


        private void SelectedScaleOnNewWeightInDisplayUnit(WeightInformation weight)
        {
            ZeroVisibility = weight.SelectedScaleIsZeroAble() ? Visibility.Visible : Visibility.Hidden;
            Debug.WriteLine(weight.NetWeight);
        }

        public ICommand ZeroCommand => new DelegateCommand(ZeroCommandExecute);

        private async void ZeroCommandExecute()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
            await _scaleService.SelectedScale.ZeroAsync();
     
        }

        public ICommand TareCommand => new DelegateCommand(TareCommandExecute);

        private async void TareCommandExecute()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
            await _scaleService.SelectedScale.TareAsync();
        }

        public ICommand ClearTareCommand => new DelegateCommand(ClearTareCommandExecute);

        private async void ClearTareCommandExecute()
        {
            await _scaleService.SelectedScale.ClearTareAsync();
        }

        public ICommand SelectScaleCommand => new DelegateCommand(SelectScaleCommandExecute);
        

        private async void SelectScaleCommandExecute()
        {

            await _scaleService.SwitchSelectedScaleAsync();
            if  (_scaleService.SelectedScale.ScaleError == ScaleErrors.ScaleNotConnected)
                await _scaleService.SwitchSelectedScaleAsync();
        }

        private Visibility _selectedScaleVisibility;

        public Visibility SelectedScaleVisibility
        {
            get { return _selectedScaleVisibility; }
            set
            {
                if (value != _selectedScaleVisibility)
                {
                    _selectedScaleVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private Visibility _zeroVisibility;

        public Visibility ZeroVisibility
        {
            get { return _zeroVisibility; }
            set
            {
                if (value != _zeroVisibility)
                {
                    _zeroVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}