﻿using System;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.GUPAS.Logic;
using System.Linq;
using System.Threading.Tasks;
using MT.GUPAS.Config;
using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace MT.GUPAS.Barcode
{
    public class BarcodeReader
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(BarcodeReader);

        private readonly ISignalManager _signalManager;
        private readonly IComponents _customerComponent;
        private string Big_data = "";
        private int _containerNumberSize;

        private BarcodeReader(ISignalManager signalManager, Components customerComponents)
        {
            _signalManager = signalManager;
            _customerComponent = customerComponents;
        }

        public static async Task<BarcodeReader> CreateAsync(ISignalManager signalManager, Components customerComponents)
        {
            var newInstance = new BarcodeReader(signalManager, customerComponents);
            await newInstance.InitializeAsync();
            await newInstance.Start();
            return newInstance;
        }

        private async Task InitializeAsync()
        {
            var configuration = await _customerComponent.GetConfigurationAsync();
        }

        public async Task Start()
        {

            var _platformEngine = ApplicationBootstrapperBase.CompositionContainer.Resolve<IPlatformEngine>();
            var _interfaceService = await _platformEngine.GetInterfaceServiceAsync();
            var _allInterfaces = await _interfaceService.GetAllInterfacesAsync();
            try
            {
                ISerialInterface _allSerialInterfaces = _allInterfaces.OfType<ISerialInterface>()
                .FirstOrDefault(item => item.LogicalPort == 6);

                if (_allSerialInterfaces == null)
                {
                    Logger.Error("Error: serial port 6 not installed");
                }
                else
                {
                    IConnectionChannel<DataSegment> _actualCommunicationChannel =
                        await _allSerialInterfaces.CreateConnectionChannelAsync();
                    DelimiterSerializer _delimiterSerializer =
                        new DelimiterSerializer(_actualCommunicationChannel, CommonDataSegments.Cr);
                    var _stringSerializer = new StringSerializer(_delimiterSerializer);

                    if (_stringSerializer != null)
                    {
                        var _eventEmiiter = new ChannelEventEmitter<string>(_stringSerializer);
                        if (_stringSerializer.IsOpen)
                            await _stringSerializer.CloseAsync();

                        await _stringSerializer.OpenAsync();
                        _eventEmiiter.MessageRead += BarcodeSerialPortOnDataReceived;
                    }

                    Logger.InfoEx($"serial if on {_allSerialInterfaces.LogicalPort} found", SourceClass);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorEx($"Exception, no serial if found", SourceClass, ex);
            }
        }

        private void BarcodeSerialPortOnDataReceived(object sender, MessageReadEventArgs<string> e)
        {
            IBarcodeDataParser barcodeDataParser = null;

            Big_data += e.Message;
            Big_data = Big_data.Trim();
            Logger.InfoEx($"data received : {Big_data}", SourceClass);
            if (string.IsNullOrEmpty(Big_data) == false)
            {
                barcodeDataParser = new BarcodeContainer(_signalManager);
            }
            barcodeDataParser?.ParseData(Big_data.Trim());
            Big_data = "";


        }
    }



}

