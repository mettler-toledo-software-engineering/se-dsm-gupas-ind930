﻿using System;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.GUPAS.Logic;
using System.IO.Ports;
using System.Management;
using System.Threading.Tasks;
using MT.GUPAS.Config;

namespace MT.GUPAS.Barcode
{
    // [InjectionBehavior(IsSingleton = true)]
    public class ZebraBarcodeReader
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(ZebraBarcodeReader);

        private string _port;
        private readonly ISignalManager _signalManager;
        private readonly IComponents _customerComponent;
        private SerialPort _serialConnection = new SerialPort();
        private int _containerNumberSize;
        private string Big_data = "";

        private ZebraBarcodeReader(ISignalManager signalManager, Components customerComponents)
        {
            _signalManager = signalManager;
            _customerComponent = customerComponents;
        }

        public static async Task<ZebraBarcodeReader> CreateAsync(ISignalManager signalManager,
            Components customerComponents)
        {
            var newInstance = new ZebraBarcodeReader(signalManager, customerComponents);
            await newInstance.InitializeAsync();
            newInstance.Start();
            return newInstance;
        }

        private async Task InitializeAsync()
        {
            var configuration = await _customerComponent.GetConfigurationAsync();
            _port = "COM7";
        }

        public void Start()
        {
            _serialConnection.BaudRate = 9600;
            _serialConnection.Parity = Parity.None;
            _serialConnection.DataBits = 8;
            _serialConnection.StopBits = StopBits.One;
            _serialConnection.Handshake = Handshake.None;
            _serialConnection.DataReceived -= BarcodeSerialPortOnDataReceived;

            _serialConnection.DataReceived -= BarcodeSerialPortOnDataReceived;
            try
            {
                if (_port != "")
                {
                    if (_serialConnection.IsOpen)
                    {
                        _serialConnection.DataReceived -= BarcodeSerialPortOnDataReceived;
                        _serialConnection.Close();
                    }

                    _serialConnection.PortName = _port;
                    _serialConnection.Open();
                    _serialConnection.DataReceived += BarcodeSerialPortOnDataReceived;
                }
            }

            catch (Exception ex)
            {
                Logger.ErrorEx($"USB Scanner on {_port} not present", SourceClass, ex);
                _port = "";
            }
        }

        private void BarcodeSerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            IBarcodeDataParser barcodeDataParser = null;
            Big_data += _serialConnection.ReadExisting();
            try
            {
                if (Big_data.Contains("\r"))
                {
                    Big_data = Big_data.Trim();
                    if (string.IsNullOrEmpty(Big_data) == false)
                    {
                        barcodeDataParser = new BarcodeContainer(_signalManager);
                    }

                    barcodeDataParser?.ParseData(Big_data);
                    Big_data = "";
                }

            }

            catch (Exception ex)
            {
                Logger.ErrorEx($"Decoding of data '{Big_data}' failed", SourceClass, ex);
            }
        }
    }
}
