﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.GUPAS.Barcode
{
    interface IBarcodeDataParser
    {
        void ParseData(string data);
    }
}
