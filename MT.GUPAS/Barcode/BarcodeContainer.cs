﻿using MT.Singularity.Composition;
using MT.GUPAS.Signals;

namespace MT.GUPAS.Barcode
{
    public class BarcodeContainer : IBarcodeDataParser
    {
        private ISignalManager _signalManager;
        
        public BarcodeContainer(ISignalManager signalManager)
        {
           _signalManager = signalManager;
        }

        public void ParseData(string data)
        {
            SendSignal(data);
        }

        private void SendSignal(string data)
        {
            IBarcodeReaderSignal signal = new ScannedValueSignal();
            int len = data.Length;
            signal.Value = data;
            _signalManager.Send(this, signal);
        }
    }
}
