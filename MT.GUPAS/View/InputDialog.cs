﻿using System;
using MT.GUPAS.View;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Input;

namespace MT.Singularity.Platform.CommonUX.Controls
{
    /// <summary>
    /// Interaction logic for NumericInputDialog
    /// </summary>
    public partial class InputDialog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NumericInputDialog"/> class.
        /// </summary>
        /// 
        public InputDialog(Layout layout, string title, string value = "")
        {
            Initialize(layout);
            Title = title;
            Value = value;
        }

        public InputDialog(Layout layout, string title, bool password, string value = "")
        {
            Initialize(layout);
            myTextBox.PasswordChar = '*';
            Title = title;
            Value = value;
        }

        private void Initialize(Layout layout)
        {
            InitializeComponents();
            switch (layout)
            {
                case Layout.Numeric:
                    KeyboardLayoutSet = EnglishKeyboardLayoutSet.NumericCancelSaveLayoutSet;
                    break;
                case Layout.AlphaNumeric:
                    KeyboardLayoutSet = EnglishKeyboardLayoutSet.AlphaNumeric;

                    break;
                default:
                    break;
            }
         }

      
        /// <summary>
        /// Gets or sets the title of the dialog.
        /// </summary>
        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the title of the dialog.
        /// </summary>
        /// 
        KeyboardLayoutSet keyboardLayoutSet = EnglishKeyboardLayoutSet.AlphaNumeric;
        public KeyboardLayoutSet KeyboardLayoutSet
        {
            get { return keyboardLayoutSet; }
            set
            {
                if (keyboardLayoutSet != value)
                {
                    keyboardLayoutSet = value;
                    NotifyPropertyChanged();
                }
            }
        }
        /// <summary>
        /// Gets or sets the value that is shown in the text box and can be changed
        /// </summary>
        public string Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    NotifyPropertyChanged();
                    OnValueUpdated(new ValueUpdatedEventArgs(value));
                }
            }
        }
        private string _value = string.Empty;

        /// <summary>
        /// Raises the <see cref="Shown" /> event
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            myTextBox.Focus();
        }

        protected void OnKeyUp(object sender, Singularity.Presentation.Input.KeyEventArgs e)
        {
            if (e.Key == Keys.Enter)
            {
                DialogResult = DialogResult.OK;
                Close();
            }

            if (e.Key == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
            else
                base.OnKeyUp(e);
        }

        /// <summary>
        /// Called when the cancel key was pressed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnCancelRequested(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Called when the save key was pressed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnCloseRequested(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Called when the save key was pressed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnSaveRequested(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        public enum Layout
        {
            Numeric,
            AlphaNumeric
        }

        /// <summary>
        /// Occures when key was pressed
        /// </summary>
        public event EventHandler<ValueUpdatedEventArgs> ValueUpdated;

        /// <summary>
        /// Raises the <see cref="ValueUpdated"/> event
        /// </summary>
        /// <param name="e">An <see cref="ValueUpdatedEventArgs"/> that contains the event data.</param>
        protected virtual void OnValueUpdated(ValueUpdatedEventArgs e)
        {
            if (ValueUpdated != null)
                ValueUpdated(this, e);
        }

        /// <summary>
        /// Contains all information related to a ValueUpdated event
        /// </summary>
        public class ValueUpdatedEventArgs : EventArgs
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ValueUpdated"/> class
            /// </summary>
            /// 
            public string Value { get; set; }
            public ValueUpdatedEventArgs(string Value)
            {
                this.Value = Value;
            }
        }


    }
}
