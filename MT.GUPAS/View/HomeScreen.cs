﻿using System.Runtime.CompilerServices;
using MT.GUPAS.Config;
using MT.GUPAS.ViewModel;
using MT.GUPAS.Infrastructure;

using MT.Singularity.Composition;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation;
using MT.GUPAS.View;


namespace MT.GUPAS.View
{
    /// <summary>
    /// Interaction logic for homeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
       private CompositionContainer _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen(CompositionContainer container)
        {
            _container = container;
            InitializeComponents();
        }

        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }


        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        /// <param name="rootVisualProvider">The root visual provider of the application.</param>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)

        {
        }


        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor
        {
#if DEBUG
            get { return false; }
#else
            get { return true; }
#endif
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _container.AddInstance<INavigationService>(NavigationService);

            NavigationService.NavigateTo(_container.Resolve<StartPage>());
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage"/>.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>

        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            return result;
        }
    }
}
