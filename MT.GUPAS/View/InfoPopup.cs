﻿using System.Threading.Tasks;
using MT.GUPAS.ViewModel;
using MT.Singularity.Presentation.Controls;

namespace MT.GUPAS.View
{
    /// <summary>
    /// Interaction logic for RecipeInformation
    /// </summary>
    public partial class InfoPopup
    {
        public readonly InfoPopupViewModel ViewModel;

        public InfoPopup(string title, string message)
        {
            ViewModel = new InfoPopupViewModel(this, title, message);

            InitializeComponents();
        }

        public InfoPopup(string title, string message, MessageBoxIcon icon)
        {
            ViewModel = new InfoPopupViewModel(this, title, message, icon);

            InitializeComponents();
        }

        public InfoPopup(string title, string message, MessageBoxIcon icon, MessageBoxButtons buttons)
        {
            ViewModel = new InfoPopupViewModel(this, title, message, icon, buttons);

            InitializeComponents();
        }

        public static Task<DialogResult> ShowAsync(Visual parent, string title, string message, MessageBoxIcon icon,
            MessageBoxButtons buttons)
        {
            var tcs = new TaskCompletionSource<DialogResult>();
            var popup = new InfoPopup(title, message, icon, buttons);
            popup.Closed += (_, __) => tcs.SetResult(popup.ViewModel.DialogResult);
            popup.Show(parent);
            return tcs.Task;
        }
    }
}