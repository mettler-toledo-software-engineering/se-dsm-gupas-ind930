﻿using MT.GUPAS.Logic;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.UserManagement;
using MT.GUPAS.ViewModel;
using MT.Singularity.Presentation.Controls.Navigation;

namespace MT.GUPAS.View
{
    /// <summary>
    /// Interaction logic for StartPage
    /// </summa
    [Export]
    public partial class StartPage
    {
        private StartPageViewModel _startPageViewModel;

        private ISignalManager _signalManager;
        private IScaleService _scaleService;
        private CompositionContainer _container;
        private Context _context;
        private INavigationService _navigationService;

        public StartPage(CompositionContainer container)
        {
            
            _container = container;
            _context = _container.Resolve<Context>();
            _signalManager = _container.Resolve<SignalManager>();
            _scaleService = _container.Resolve<IScaleService>();
            _navigationService = _container.Resolve<INavigationService>();          
            _startPageViewModel = new StartPageViewModel(this, _container, _navigationService);
            InitializeComponents();
            _context.scalePanelView = new ScalePanel(_scaleService, _signalManager);
            _scalePanel.Add(_context.scalePanelView);

        }



        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage"/>.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            _context.scalePanelView.DeactivateWeightWindow();
            _scalePanel.Remove(_context.scalePanelView);
            var result = base.OnNavigatingAway(nextPage);
            if (result == NavigationResult.Proceed)
            {

            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            _scalePanel.Add(_context.scalePanelView);
            _context.scalePanelView.ActivateWeightWindow();
            base.OnNavigationReturning(previousPage);

        }


    }
}
