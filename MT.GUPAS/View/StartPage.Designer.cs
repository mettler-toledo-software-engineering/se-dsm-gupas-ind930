﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Presentation.Drawing;
namespace MT.GUPAS.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class StartPage : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.Grid.DynamicGrid _scalePanel;
        private MT.Singularity.Presentation.Controls.StackPanel PageLogo;
        private MT.Singularity.Presentation.Controls.TextBlock lblVersion;
        private MT.Singularity.Presentation.Controls.GroupPanel ButtonPanel;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Grid.Grid internal1;
            MT.Singularity.Presentation.Controls.GroupPanel internal2;
            MT.Singularity.Presentation.Controls.Image internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.Button internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.Image internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            _scalePanel = new MT.Singularity.Presentation.Controls.Grid.DynamicGrid();
            internal2 = new MT.Singularity.Presentation.Controls.GroupPanel(_scalePanel);
            internal2.Height = 200;
            internal2.GridColumn = 0;
            internal2.GridColumnSpan = 2;
            internal2.GridRow = 0;
            internal3 = new MT.Singularity.Presentation.Controls.Image();
            internal3.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.MTLogo.png";
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal3.Margin = new MT.Singularity.Presentation.Thickness(6);
            lblVersion = new MT.Singularity.Presentation.Controls.TextBlock();
            lblVersion.Margin = new MT.Singularity.Presentation.Thickness(16, 6, 0, 0);
            lblVersion.FontSize = ((System.Nullable<System.Int32>)12);
            lblVersion.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => lblVersion.Text,() =>  _startPageViewModel.ProgVersion,MT.Singularity.Expressions.BindingMode.OneWay,false);
            PageLogo = new MT.Singularity.Presentation.Controls.StackPanel(internal3, lblVersion);
            PageLogo.GridColumn = 0;
            PageLogo.GridRow = 1;
            PageLogo.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(270, 50, 0, 0);
            internal5.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.DSMLogo.png";
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal4.GridColumn = 1;
            internal4.GridRow = 1;
            internal7 = new MT.Singularity.Presentation.Controls.Button();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal7.Width = 200;
            internal7.Height = 90;
            internal7.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal7.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal9 = new MT.Singularity.Presentation.Controls.Image();
            internal9.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.ArrowRight.al8";
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.StartProgram);
            internal10.AddTranslationAction(() => {
                internal10.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.StartProgram);
            });
            internal10.FontSize = ((System.Nullable<System.Int32>)18);
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(internal9, internal10);
            internal7.Content = internal8;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Command,() => _startPageViewModel.GoContinue,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7);
            internal6.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            ButtonPanel = new MT.Singularity.Presentation.Controls.GroupPanel(internal6);
            ButtonPanel.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            ButtonPanel.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4279383126u));
            ButtonPanel.GridColumn = 0;
            ButtonPanel.GridColumnSpan = 2;
            ButtonPanel.GridRow = 2;
            internal1 = new MT.Singularity.Presentation.Controls.Grid.Grid(internal2, PageLogo, internal4, ButtonPanel);
            internal1.RowDefinitions = GridDefinitions.Create("200","1*","100");
            internal1.ColumnDefinitions = GridDefinitions.Create("280", "1*");
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[2];
    }
}
