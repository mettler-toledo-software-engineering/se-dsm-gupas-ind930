﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MT.GUPAS.Data;
using MT.GUPAS.Infrastructure;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using MT.Singularity.Composition;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Presentation.Input;


namespace MT.GUPAS.View
{
    /// <summary>
    /// Interaction logic for Login
    /// </summary>
    public partial class Login
    {
        Visual _parent;


        public Login(Visual parent, CompositionContainer container)
        {
            _container = container;
            _parent = parent;

            Task.Run(new Action(() =>
            {
                Initialize("");
            }));
        }


        private async void Initialize(string username)
        {
            InitializeComponents();
            tBPassword.PasswordChar = '*';
            tbUser.GotFocus += tbUser_GotFocus;
            tBPassword.GotFocus += tBPassword_GotFocus;
            KeyUp += Login_KeyUp;


            // start user/password input directly
            if (String.IsNullOrEmpty(username))
            {
                if (await UserEntry() == DialogResult.OK)
                    await PasswordEntry();
            }
            else
            {
                LoginName = username;
                await PasswordEntry();
            }
        }

        void Login_KeyUp(object sender, Singularity.Presentation.Input.KeyEventArgs e)
        {
            if (e.Key == Keys.Enter)
            {
                DoCmdOk();
            }

            if (e.Key == Keys.Escape)
            {
                DoCmdCancel();
            }
        }

        void tBPassword_GotFocus(object sender, EventArgs e)
        {
            PasswordEntry();
        }

        void tbUser_GotFocus(object sender, EventArgs e)
        {
            UserEntry();
        }

        public async Task<DialogResult> UserEntry()
        {
            var nd = new InputDialog(InputDialog.Layout.AlphaNumeric, "LoginName");
            nd.Width = 1100;
#if DEBUG
            nd.Value = "5179";
#endif        

            var dialogResult = await nd.ShowAsync(_parent);
            if (dialogResult == DialogResult.OK)
            {
                LoginName = nd.Value;
            }
            return dialogResult;
        }

        public async Task<DialogResult> PasswordEntry()
        {
            var nd = new InputDialog(InputDialog.Layout.AlphaNumeric, "Passwort", true);
            nd.Width = 1100;
#if DEBUG
            nd.Value = "121212";
#endif        
            var dialogResult = await nd.ShowAsync(_parent);
            if (dialogResult == DialogResult.OK)
            {
                Password = nd.Value;
            }
            return dialogResult;
        }

        #region Props

        string loginName = "";
        public string LoginName
        {
            get { return loginName; }
            set
            {
                if (value != null)
                {
                    loginName = value;
                    NotifyPropertyChanged();
                }
            }
        }

        string password = "";
        private CompositionContainer _container;

        public string Password
        {
            get { return password; }
            set
            {
                if (value != null)
                {
                    password = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Commands

        public ICommand CmdCancel
        {
            get { return new DelegateCommand(DoCmdCancel); }
        }

        void DoCmdCancel()
        {
            DialogResult = DialogResult.Cancel;
            Close();

        }

        public ICommand CmdOk
        {
            get { return new DelegateCommand(DoCmdOk); }
        }

        async void DoCmdOk()
        {
            var context = _container.Resolve<Context>();

            context._selectedUser = null;

            if (Password == "248163264")
            {
                context._selectedUser = new UserModel()
                {
                    Tag = "MTCH",
                    Nummer = 0,
                    Account = 4334,
                    PW = Password,
                    Name = "Mettler-Toledo",
                    Level = (int)Globals.PermissionLevels.Administrator
                };
                AccessOk();
            }
            else
            {
                if (String.IsNullOrEmpty(LoginName))
                {
                    await InfoPopup.ShowAsync(_parent, "Fehler", "Username fehlt", MessageBoxIcon.Error, MessageBoxButtons.OK);
                    return;
                }

                var _userBaseConnection = _container.Resolve<SqlGenericDataBaseAccess<UserModel>>();
             
                context._selectedUser = _userBaseConnection.GetEntriesByParameter("Account", LoginName).FirstOrDefault();
                if (context._selectedUser != null)
                {
                    if (context._selectedUser.PW.Trim() == Password.Trim())
                    {
                        AccessOk();
                    }
                    else
                        AccessDenied();
                }
                else
                    AccessDenied();
            }
        }

        private async void AccessOk()
        {
        //    await InfoPopup.ShowAsync(_parent, "Erfolg", "Zugriff ok!", MessageBoxIcon.Information, MessageBoxButtons.OK);
            DialogResult = DialogResult.OK;
            Close();

        }

        private async void AccessDenied()
        {
            await InfoPopup.ShowAsync(_parent, "Fehler", "Zugriff abgelehnt!", MessageBoxIcon.Warning, MessageBoxButtons.OK);
            DialogResult = DialogResult.Cancel;

        }
        #endregion
    }
}
