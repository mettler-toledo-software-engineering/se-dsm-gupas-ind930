﻿using MT.GUPAS.Infrastructure;
using MT.GUPAS.Logic;
using MT.GUPAS.Models;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.GUPAS.ViewModel;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Input;

namespace MT.GUPAS.View
{
    /// <summary>
    /// Interaction logic for MainScreen
    /// </summary>
    [Export(typeof(Dosing))]
    public partial class Dosing
    {
        private readonly DosingViewModel _viewModel;
        private CompositionContainer _container;
        private INavigationService _navigationService;
        private Context _context;


        public Dosing(CompositionContainer container)
        {
            _container = container;
            _context = _container.Resolve<Context>();
            _navigationService = _container.Resolve<INavigationService>();
            _viewModel = new DosingViewModel(this, _container, _navigationService);
            InitializeComponents();
        }

        #region Overrides

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _scalePanel.Add(_context.scalePanelView);
            _context.scalePanelView.ActivateWeightWindow();
            PointerDown += Dosing_PointerDown;
            base.OnFirstNavigation();
        }

        private void Dosing_PointerDown(object sender, PointerEventArgs e)
        {
            _viewModel.RenewTimeLeft();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _context.scalePanelView.DeactivateWeightWindow();
            _scalePanel.Remove(_context.scalePanelView); PointerDown -= Dosing_PointerDown;
            return base.OnNavigatingBack(nextPage);
        }

        
        #endregion


    }
}
