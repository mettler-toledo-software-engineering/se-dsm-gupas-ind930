﻿using MT.GUPAS.ViewModel;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace MT.GUPAS.View
{
    /// <summary>
    /// Interaction logic for ScalePanel
    /// </summary>
    public partial class ScalePanel 
    {
        private ScalePanelViewModel _scalePanelViewModel;
        private IScaleService _scaleService;
        public ScalePanel(IScaleService scaleService, ISignalManager signalManager)
        {
            _scaleService = scaleService;
            _scalePanelViewModel = new ScalePanelViewModel(this, signalManager, _scaleService);
            InitializeComponents();
            
            
            _weightDisplayWindow.Activate();
        }

        public void DeactivateWeightWindow()
        {
            _weightDisplayWindow.Deactivate();
        }

        public void ActivateWeightWindow()
        {
            _weightDisplayWindow.Activate();
        }
  
    }
}
