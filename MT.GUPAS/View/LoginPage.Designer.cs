﻿using MT.Singularity.Presentation.Controls;
namespace MT.GUPAS.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class Login : MT.Singularity.Presentation.Controls.ChildWindow
    {
        public void UpdatetBPasswordPasswordChar()
        {
            tBPassword.PasswordChar = '*';
        }
        private MT.Singularity.Presentation.Controls.TextBox tbUser;
        private MT.Singularity.Presentation.Controls.TextBox tBPassword;
        private MT.Singularity.Presentation.Controls.Button cmdOk;
        private MT.Singularity.Presentation.Controls.Button cmdCancel;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.GroupPanel internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.FontSize = ((System.Nullable<System.Int32>)40);
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4.Width = 200;
            internal4.Text = "Benutzer:";
            tbUser = new MT.Singularity.Presentation.Controls.TextBox();
            tbUser.ShowKeyboardOnFocus = false;
            tbUser.Margin = new MT.Singularity.Presentation.Thickness(10);
            tbUser.FontSize = ((System.Nullable<System.Int32>)40);
            tbUser.Width = 420;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => tbUser.Text,() => LoginName,MT.Singularity.Expressions.BindingMode.OneWay,false);
            tbUser.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.AlphaNumeric;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, tbUser);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.FontSize = ((System.Nullable<System.Int32>)40);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal6.Width = 200;
            internal6.Text = "Passwort:";
            tBPassword = new MT.Singularity.Presentation.Controls.TextBox();
            tBPassword.ShowKeyboardOnFocus = false;
            tBPassword.Margin = new MT.Singularity.Presentation.Thickness(10);
            tBPassword.FontSize = ((System.Nullable<System.Int32>)40);
            tBPassword.Width = 420;
            this.UpdatetBPasswordPasswordChar();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => tBPassword.Text,() =>  Password,MT.Singularity.Expressions.BindingMode.OneWay,false);
            tBPassword.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.AlphaNumeric;
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, tBPassword);
            internal5.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            cmdOk = new MT.Singularity.Presentation.Controls.Button();
            cmdOk.Width = 192;
            cmdOk.Height = 80;
            cmdOk.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            cmdOk.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.ok.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.Text = "Ok";
            internal12.FontSize = ((System.Nullable<System.Int32>)28);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11, internal12);
            cmdOk.Content = internal10;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdOk.Command,() =>  CmdOk,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(cmdOk);
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            cmdCancel = new MT.Singularity.Presentation.Controls.Button();
            cmdCancel.Width = 192;
            cmdCancel.Height = 80;
            cmdCancel.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            cmdCancel.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.Cancel.al8";
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.Text = "Cancel";
            internal16.FontSize = ((System.Nullable<System.Int32>)28);
            internal16.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal14 = new MT.Singularity.Presentation.Controls.GroupPanel(internal15, internal16);
            cmdCancel.Content = internal14;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdCancel.Command,() =>  CmdCancel,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(cmdCancel);
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(internal9, internal13);
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal8.Width = 630;
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(internal8);
            internal7.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal7.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal5, internal7);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal2.Margin = new MT.Singularity.Presentation.Thickness(20);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
