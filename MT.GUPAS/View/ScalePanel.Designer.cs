﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;

namespace MT.GUPAS.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class ScalePanel : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Presentation.Controls.Button cmdTare;
        private MT.Singularity.Presentation.Controls.Button cmdClrTare;
        private MT.Singularity.Presentation.Controls.TextBlock cmdClrTareCaption;
        private MT.Singularity.Presentation.Controls.Button cmdSelectScale;
        private MT.Singularity.Presentation.Controls.TextBlock cmdSelectScaleCaption;
        private MT.Singularity.Presentation.Controls.Button cmdZero;
        private MT.Singularity.Presentation.Controls.TextBlock cmdZeroCaption;
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow _weightDisplayWindow;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.GroupPanel internal3;
            MT.Singularity.Presentation.Controls.Image internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.GroupPanel internal6;
            MT.Singularity.Presentation.Controls.Image internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.GroupPanel internal9;
            MT.Singularity.Presentation.Controls.Image internal10;
            MT.Singularity.Presentation.Controls.GroupPanel internal11;
            MT.Singularity.Presentation.Controls.Image internal12;
            MT.Singularity.Presentation.Controls.DockPanel internal13;
            cmdTare = new MT.Singularity.Presentation.Controls.Button();
            cmdTare.Margin = new MT.Singularity.Presentation.Thickness(6, 6, 3, 3);
            cmdTare.Width = 150;
            cmdTare.Height = 91;
            cmdTare.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            cmdTare.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal4 = new MT.Singularity.Presentation.Controls.Image();
            internal4.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.Tare.al8";
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal5.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.Tara);
            internal5.AddTranslationAction(() => {
                internal5.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.Tara);
            });
            internal5.FontSize = ((System.Nullable<System.Int32>)12);
            internal5.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal3 = new MT.Singularity.Presentation.Controls.GroupPanel(internal4, internal5);
            cmdTare.Content = internal3;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdTare.Command,() =>  _scalePanelViewModel.TareCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            cmdClrTare = new MT.Singularity.Presentation.Controls.Button();
            cmdClrTare.Margin = new MT.Singularity.Presentation.Thickness(6, 3, 3, 3);
            cmdClrTare.Width = 150;
            cmdClrTare.Height = 91;
            cmdClrTare.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            cmdClrTare.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal7 = new MT.Singularity.Presentation.Controls.Image();
            internal7.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.ClearTare.al8";
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            cmdClrTareCaption = new MT.Singularity.Presentation.Controls.TextBlock();
            cmdClrTareCaption.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            cmdClrTareCaption.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.TaraLoeschen);
            cmdClrTareCaption.AddTranslationAction(() => {
                cmdClrTareCaption.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.TaraLoeschen);
            });
            cmdClrTareCaption.FontSize = ((System.Nullable<System.Int32>)12);
            cmdClrTareCaption.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal6 = new MT.Singularity.Presentation.Controls.GroupPanel(internal7, cmdClrTareCaption);
            cmdClrTare.Content = internal6;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdClrTare.Command,() =>  _scalePanelViewModel.ClearTareCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(cmdTare, cmdClrTare);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            cmdSelectScale = new MT.Singularity.Presentation.Controls.Button();
            cmdSelectScale.Margin = new MT.Singularity.Presentation.Thickness(3, 6, 0, 3);
            cmdSelectScale.Width = 150;
            cmdSelectScale.Height = 91;
            cmdSelectScale.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            cmdSelectScale.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal10 = new MT.Singularity.Presentation.Controls.Image();
            internal10.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.Scale.al8";
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            cmdSelectScaleCaption = new MT.Singularity.Presentation.Controls.TextBlock();
            cmdSelectScaleCaption.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            cmdSelectScaleCaption.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.WaageAuswaehlen);
            cmdSelectScaleCaption.AddTranslationAction(() => {
                cmdSelectScaleCaption.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.WaageAuswaehlen);
            });
            cmdSelectScaleCaption.FontSize = ((System.Nullable<System.Int32>)12);
            cmdSelectScaleCaption.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal9 = new MT.Singularity.Presentation.Controls.GroupPanel(internal10, cmdSelectScaleCaption);
            cmdSelectScale.Content = internal9;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdSelectScale.Visibility,() =>  _scalePanelViewModel.SelectedScaleVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdSelectScale.Command,() =>  _scalePanelViewModel.SelectScaleCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            cmdZero = new MT.Singularity.Presentation.Controls.Button();
            cmdZero.Margin = new MT.Singularity.Presentation.Thickness(3, 3, 0, 0);
            cmdZero.Width = 150;
            cmdZero.Height = 91;
            cmdZero.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            cmdZero.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal12 = new MT.Singularity.Presentation.Controls.Image();
            internal12.Source = "embedded://MT.GUPAS/MT.GUPAS.Images.Zero.al8";
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            cmdZeroCaption = new MT.Singularity.Presentation.Controls.TextBlock();
            cmdZeroCaption.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            cmdZeroCaption.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.Nullieren);
            cmdZeroCaption.AddTranslationAction(() => {
                cmdZeroCaption.Text = MT.GUPAS.Localization.Get(MT.GUPAS.Localization.Key.Nullieren);
            });
            cmdZeroCaption.FontSize = ((System.Nullable<System.Int32>)12);
            cmdZeroCaption.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal11 = new MT.Singularity.Presentation.Controls.GroupPanel(internal12, cmdZeroCaption);
            cmdZero.Content = internal11;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdZero.Visibility,() =>  _scalePanelViewModel.ZeroVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => cmdZero.Command,() =>  _scalePanelViewModel.ZeroCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(cmdSelectScale, cmdZero);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            _weightDisplayWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            _weightDisplayWindow.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _weightDisplayWindow.Height = 200;
            _weightDisplayWindow.Width = 600;
            _weightDisplayWindow.Background = new MT.Singularity.Presentation.Drawing.LinearGradientBrush(new MT.Singularity.Presentation.Drawing.GradientStop(0d, new MT.Singularity.Presentation.Color(4290821308u)), new MT.Singularity.Presentation.Drawing.GradientStop(0.5d, new MT.Singularity.Presentation.Color(4284176729u)), new MT.Singularity.Presentation.Drawing.GradientStop(1d, new MT.Singularity.Presentation.Color(4290821308u))) {
                Orientation = MT.Singularity.Presentation.Orientation.Vertical,

            };
            _weightDisplayWindow.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _weightDisplayWindow.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            internal13 = new MT.Singularity.Presentation.Controls.DockPanel(_weightDisplayWindow);
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal8, internal13);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1.Background = new MT.Singularity.Presentation.Drawing.LinearGradientBrush(new MT.Singularity.Presentation.Drawing.GradientStop(0d, new MT.Singularity.Presentation.Color(4290821308u)), new MT.Singularity.Presentation.Drawing.GradientStop(0.5d, new MT.Singularity.Presentation.Color(4284176729u)), new MT.Singularity.Presentation.Drawing.GradientStop(1d, new MT.Singularity.Presentation.Color(4290821308u))) {
                Orientation = MT.Singularity.Presentation.Orientation.Vertical,

            };
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal1.Width,() =>  Logic.Globals.ScreenWidth,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal1.Height = 200;
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            this.FontSize = ((System.Nullable<System.Int32>)24);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[7];
    }
}
