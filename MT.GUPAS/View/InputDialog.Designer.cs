﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Input;
namespace MT.Singularity.Platform.CommonUX.Controls
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class InputDialog : MT.Singularity.Presentation.Controls.ChildWindow
    {
        public void UpdatemyTextBoxFont()
        {
            myTextBox.Font = MT.Singularity.Presentation.Drawing.Font.ArialFont;
        }
        private MT.Singularity.Presentation.Controls.TextBox myTextBox;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.TextBlock internal2;
            MT.Singularity.Presentation.Controls.Keyboard internal3;
            internal2 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal2.Text,() =>  Title,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 10);
            myTextBox = new MT.Singularity.Presentation.Controls.TextBox();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => myTextBox.Text,() =>  Value,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            myTextBox.ShowKeyboardOnFocus = false;
            myTextBox.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            this.UpdatemyTextBoxFont();
            myTextBox.FontSize = ((System.Nullable<System.Int32>)26);
            myTextBox.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleLeft;
            myTextBox.KeyUp += OnKeyUp;
            myTextBox.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 10);
            internal3 = new MT.Singularity.Presentation.Controls.Keyboard();
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.KeyboardLayoutSet,() =>  KeyboardLayoutSet,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.CancelRequested += OnCancelRequested;
            internal3.CloseRequested += OnCloseRequested;
            internal3.SaveRequested += OnSaveRequested;
            internal3.Height = 416;
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, myTextBox, internal3);
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            this.BorderBrush = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278190080u));
            this.BorderThickness = 1;
            this.Padding = new MT.Singularity.Presentation.Thickness(10);
            this.Width = 704;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
