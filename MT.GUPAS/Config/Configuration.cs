﻿using System.ComponentModel;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace MT.GUPAS.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {
        public Configuration()
        {
        }

        
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(1)]
        public virtual int TerminalId
        {
            get { return (int)GetLocal(); }
            set
            {
                SetLocal(value); 
                
            }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(900)]
        public virtual int AutoLogoffTime
        {
            get { return (int)GetLocal(); }
            set
            {
                SetLocal(value);

            }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string UsbUpdateResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

    }
}
