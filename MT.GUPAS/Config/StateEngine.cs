﻿namespace MT.GUPAS.Config
{
      enum StateEngine
    {
        Init,
        WaitOnPosNr,
        WaitOnMaterialNr,
        WaitOnHumidity,
        WaitOnChargeNr,
        WaitOnContainerNr,
        WaitSecondTare,
        WaitOnLoadScale,
        Idle,
        LoadScale,
        PrepareDeltaTrac,
        Fill,
        ToleranceCheck,
        WaitOnTareZero,
        Pulsing,
        Done,
        WaitOnUnLoadScale,
        ResetScale,
        PreInit2,
        PreInit
    }
}
