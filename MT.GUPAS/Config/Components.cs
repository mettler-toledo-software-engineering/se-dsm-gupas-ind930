﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace MT.GUPAS.Config
{
    [Export(typeof(IComponents))]
    [Export(typeof(IConfigurable))]
    [Export(typeof(Components))]
    [InjectionBehavior(IsSingleton = true)]
    public class Components : ConfigurationStoreConfigurable<Configuration>, IComponents
    {
        public Components(IConfigurationStore configurationStore,
            ISecurityService securityService, CompositionContainer compositionContainer, bool traceChanges = true)
            : base(configurationStore, securityService, compositionContainer, traceChanges)
        {
        }
        //identifikationsschlüssel im Datenbank file, welches die Konfiguration abspeichert.
        public override string ConfigurationSelector
        {
            get { return "DSM-Configuration"; }
        }

        public override string FriendlyName
        {
            get { return "DSM Konfiguration"; }
        }

    }
}
