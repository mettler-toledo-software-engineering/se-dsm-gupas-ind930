﻿using MT.GUPAS.Models;
using MT.GUPAS.View;
using MT.Singularity.Composition;

namespace MT.GUPAS.Logic
{   [InjectionBehavior(IsSingleton = true)]
    [Export (typeof(Context))]
    public class Context
    {
        public UserModel _selectedUser;
        public ScalePanel scalePanelView;
        public int _aktLot;
      
    }
}
