﻿using System;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace MT.GUPAS.Logic
{
    public static class WeightExtensions
    {
        public static bool SelectedScaleIsZeroAble(this WeightInformation weight)
        {
            bool result = false;
            if (weight.IsValid)
            {
                if (weight.GetGrossWeightIn(WellknownWeightUnit.Gram) < 10 &&
                    weight.GetGrossWeightIn(WellknownWeightUnit.Gram) > -10 && weight.IsValid)
                {
                    result = true;
                }
            }

            return result;
        }

        public static double GetGrossWeightIn(this WeightInformation weight, WellknownWeightUnit unit)
        {
            return WeightUnits.Convert(weight.GrossWeight, weight.Unit, unit);
        }
         public static double GetNetWeightIn(this WeightInformation weight, WellknownWeightUnit unit)
        {
            return WeightUnits.Convert(weight.NetWeight, weight.Unit, unit);
        }
         public static double GetTareWeightIn(this WeightInformation weight, WellknownWeightUnit unit)
        {
            return WeightUnits.Convert(weight.TareWeight, weight.Unit, unit);
        }

    }
}
