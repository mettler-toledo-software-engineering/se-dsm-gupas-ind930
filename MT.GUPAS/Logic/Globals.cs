﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using MT.Singularity.Composition;
using MT.Singularity.Platform;
using System.Configuration;
using System.Linq;
using System.Net.NetworkInformation;
using log4net.Repository.Hierarchy;
using log4net.Util;

namespace MT.GUPAS.Logic
{

    [Export]
    public class Globals
    {
        private static readonly SingularityEnvironment Environment = new SingularityEnvironment("MT.GUPAS");
        public const string IND930USBDrive = "D:\\";

        public const string ProjectNumber = "P19013001";
        public const int ScreenWidth = 1280;
        public const int ScreenHeight = 800;
        public static string ProgVersionStr(bool withRevision)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            if (withRevision)
            {
                vers += "." + version.Revision.ToString();
            }

            return vers;
        }

        public const int MaxItemCode = 28;
        public const int BigFontSize = 25;
        public const int FontSize = 18;
        public const int SmallFontSize = 10;
        public const string UOM = "kg";
        public enum PermissionLevels
        {
            User = 1,
            Supervisor = 3,
            Administrator = 7,
            MTService = 255,
            NoAuth = 0
        }

        public static string GetDataDirectory()
        {
            return Environment.DataDirectory;
        }

        public static string CnnString()
        {
            //um den Configuration Manager nutzen zu können muss die Referenz System.Configuration hinzugefügt werden
            //add reference --> assemblies --> system.configuration
            return ConfigurationManager.ConnectionStrings["GUPASSqlConnection"].ConnectionString;

        }
        public static bool DBServer()
        {
            try
            {
                var ping = new Ping();
                string server = "";
                // Extraktion des Servernamens oder IP
                string lcn = ConfigurationManager.ConnectionStrings["GUPASSqlConnection"].ConnectionString;
                string dsn = lcn.Split(';').FirstOrDefault(d => d.StartsWith("Data Source")).Split('=')[1];
              

                if (dsn.Length > 0)
                {
                    server = (dsn.Split('\\').Length > 0) ? dsn.Split('\\')[0] : "";
                }


                var reply = ping.Send(server, 500);
                return (reply?.Status == IPStatus.Success) ? true : false;
            }
            catch (Exception ex)
            {
                return false;
            }



        }

    }
}
